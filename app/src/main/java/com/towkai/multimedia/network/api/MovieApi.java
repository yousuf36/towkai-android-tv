package com.towkai.multimedia.network.api;


import com.towkai.multimedia.model.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface MovieApi {

    @GET("movies")
    Call<List<Movie>> getMovies(@Header("API-KEY") String apiKey,
                                @Query("page") int page,
                                @Query("sort") String sort,
                                @Query("user_id") String user_id
                                );

    @GET("content_by_genre_id")
    Call<List<Movie>> getMovieByGenre(@Header("API-KEY") String apiKey,
                                      @Query("id") String id,
                                      @Query("page") int page_num);


    @GET("content_by_country_id")
    Call<List<Movie>> getMovieByCountry(@Header("API-KEY") String apiKey,
                                        @Query("id") String id,
                                        @Query("page") int page_number);
    @GET("home_content_movies_by_year")
    Call<List<Movie>> getMovieByYear(@Header("API-KEY") String apiKey,
                                        @Query("id") String id,
                                        @Query("page") int page_number);
    @GET("home_content_by_lan")
    Call<List<Movie>> getMovieByLanguage(@Header("API-KEY") String apiKey,
                                        @Query("id") String id,
                                        @Query("page") int page_number);

}
