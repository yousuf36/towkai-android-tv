package com.towkai.multimedia.network.api;

import com.towkai.multimedia.model.MovieSingleDetails;
import com.towkai.multimedia.model.SinglePlayBack;
import com.towkai.multimedia.model.UpdateWatchTimeResponse;

import okhttp3.Response;
import retrofit2.Call;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface DetailsApi {

    @GET("single_details")
    Call<MovieSingleDetails> getSingleDetail(@Header("API-KEY") String apiKey,
                                             @Query("type") String videoType,
                                             @Query("id") String videoId,
                                             @Query("user_id") String user_id
    );

    @GET("autoplay_tvseries")
    Call<SinglePlayBack> getSingleEpisod(@Header("API-KEY") String apiKey,
                                         @Query("type") String videoType,
                                         @Query("id") String episodId,
                                         @Query("season_id") String season_id
                                      //   @Query("user_id") String user_id
    );

    @GET("resume_history")
    Call<String> updateWatchTime(
            @Header("API-KEY") String apiKey,
//                                                  @Query("type") String videoType,
            @Query("user_id") String user_id,
            @Query("id") String videoId,
            @Query("mint") String duration,
            @Query("season_id") String season_id,
            @Query("episode_id") String episode_id
    );
}
