package com.towkai.multimedia.network.api;


import com.towkai.multimedia.model.Genre;
import com.towkai.multimedia.model.Language;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface LanguageApi {

    @GET("language")
    Call<List<Language>> getLanguage(@Header("API-KEY") String apiKey,
                                     @Query("page") int page);
}
