package com.towkai.multimedia.network.api;

import com.towkai.multimedia.model.HomeContent;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface HomeApi {

    @GET("home_content")
    Call<List<HomeContent>> getHomeContent(
            @Header("API-KEY") String apiKey,
            @Query("user_id") String userId);
}
