package com.towkai.multimedia.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.towkai.multimedia.R;
import com.towkai.multimedia.model.Subtitle;

import java.util.List;

public class SubtitleListAdapter extends RecyclerView.Adapter<SubtitleListAdapter.SubtitleViewHolder> {
    private Context context;
    private List<Subtitle> subtitleList;
    private OnSubtitleItemClickListener listener;
    private SubtitleViewHolder viewHolder;

    public SubtitleListAdapter(Context context, List<Subtitle> subtitleList) {
        this.context = context;
        this.subtitleList = subtitleList;
    }

    @NonNull
    @Override
    public SubtitleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_server_tv_item, parent, false);
        return new SubtitleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubtitleViewHolder holder, @SuppressLint("RecyclerView") int p) {
        if (p < subtitleList.size()-1) {
               holder.subtitleOffTv.setVisibility(View.GONE);
        }
        if (p==subtitleList.size()-1) {
            holder.subtitleOffTv.setVisibility(View.VISIBLE);
            holder.subtitleOffTv.setText(context.getResources().getString(R.string.turn_off));
        }



        Subtitle subtitle = subtitleList.get(p);
        holder.subtitleNameTv.setText("Turn On- "+subtitle.getLanguage());

        holder.subtitleNameTv.setOnClickListener(v -> {
            if (listener != null) {
                listener.onSubtitleItemClick(v, subtitle, p, holder);
            }
        });
        holder.subtitleOffTv.setOnClickListener(v -> {
            if (listener != null) {
                listener.onSubtitleItemClick(v, subtitle, -1, holder);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subtitleList.size();
    }

    public static class SubtitleViewHolder extends RecyclerView.ViewHolder {
        Button subtitleNameTv;
        Button subtitleOffTv;

        SubtitleViewHolder(@NonNull View itemView) {
            super(itemView);
            subtitleNameTv = itemView.findViewById(R.id.s_name_tv);
            subtitleOffTv = itemView.findViewById(R.id.s_off_tv);
        }
    }

    public interface OnSubtitleItemClickListener {
        void onSubtitleItemClick(View view, Subtitle subtitle, int position, SubtitleListAdapter.SubtitleViewHolder holder);
    }

    public void setListener(OnSubtitleItemClickListener listener) {
        this.listener = listener;
    }
}
