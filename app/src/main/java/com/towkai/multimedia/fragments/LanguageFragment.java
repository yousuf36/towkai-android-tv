package com.towkai.multimedia.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.app.VerticalGridSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.VerticalGridPresenter;

import com.towkai.multimedia.Config;
import com.towkai.multimedia.NetworkInst;
import com.towkai.multimedia.R;
import com.towkai.multimedia.model.Genre;
import com.towkai.multimedia.model.Language;
import com.towkai.multimedia.model.Year;
import com.towkai.multimedia.network.RetrofitClient;
import com.towkai.multimedia.network.api.GenreApi;
import com.towkai.multimedia.network.api.LanguageApi;
import com.towkai.multimedia.network.api.YearApi;
import com.towkai.multimedia.ui.activity.ErrorActivity;
import com.towkai.multimedia.ui.activity.ItemGenreActivity;
import com.towkai.multimedia.ui.activity.ItemLanguageActivity;
import com.towkai.multimedia.ui.activity.LeanbackActivity;
import com.towkai.multimedia.ui.presenter.GenrePresenter;
import com.towkai.multimedia.ui.presenter.LanguageCardPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class LanguageFragment extends VerticalGridSupportFragment {
    public static final String LANGUAGE = "language";
    private static final String TAG = LanguageFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 2;
    private int pageCount = 1;
    private boolean dataAvailable = true;

    //private BackgroundHelper bgHelper;
    //private List<Genre> genres = new ArrayList<>();
    private ArrayObjectAdapter mAdapter;
    private LeanbackActivity activity;
    private ArrayList<Language> list = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

//
//        list.add(new Language("1","Bangla",""));
//        list.add(new Language("2","English",""));
//        list.add(new Language("3","Hindi\t",""));
//        list.add(new Language("4","Others ",""));

        activity = (LeanbackActivity) getActivity();
        activity.hideLogo();
        setTitle(getResources().getString(R.string.language));
        activity.hideDropDown();
        //bgHelper = new BackgroundHelper(getActivity());

        setOnItemViewClickedListener(getDefaultItemViewClickedListener());
        setOnItemViewSelectedListener(getDefaultItemSelectedListener());

        // setup
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

       // mAdapter = new ArrayObjectAdapter(new GenreCardPresenter());
        mAdapter = new ArrayObjectAdapter(new LanguageCardPresenter());
        setAdapter(mAdapter);

        fetchGenreData(pageCount);

    }

    // click listener
    private OnItemViewClickedListener getDefaultItemViewClickedListener() {
        return new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder2, Row row) {
                Language language = (Language) o;
                Intent intent = new Intent(getActivity(), ItemLanguageActivity.class);
                intent.putExtra("id", language.getIso());
                intent.putExtra("title", language.getName());
                startActivity(intent);

            }
        };
    }


    // selected listener for setting blur background each time when the item will select.
    protected OnItemViewSelectedListener getDefaultItemSelectedListener() {
        return new OnItemViewSelectedListener() {
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, final Object item,
                                       RowPresenter.ViewHolder rowViewHolder, Row row) {


                if (item instanceof Genre) {
                    /*bgHelper = new BackgroundHelper(getActivity());
                    bgHelper.prepareBackgroundManager();
                    bgHelper.startBackgroundTimer(((Genre) item).getImageUrl());*/


                }
            }
        };
    }

    public void fetchGenreData(int pageCount) {

        if (!new NetworkInst(activity).isNetworkAvailable()) {
            Intent intent = new Intent(activity, ErrorActivity.class);
            startActivity(intent);
            activity.finish();
            return;
        }

        final SpinnerFragment mSpinnerFragment = new SpinnerFragment();
        final FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.custom_frame_layout, mSpinnerFragment).commit();

        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        LanguageApi api = retrofit.create(LanguageApi.class);
        Call<List<Language>> call = api.getLanguage(Config.API_KEY, pageCount);
        call.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, Response<List<Language>> response) {
                if (response.code() == 200) {
                    List<Language> languageList = response.body();
                    if (languageList.size() <= 0) {
                        dataAvailable = false;
                        Toast.makeText(activity, getResources().getString(R.string.no_data_found), Toast.LENGTH_SHORT).show();
                    }

                    for (Language lan : languageList) {
                        mAdapter.add(lan);
                        list.add(lan);
                    }

                    mAdapter.notifyArrayItemRangeChanged(languageList.size() - 1, languageList.size());
                    // hide the spinner
                    fm.beginTransaction().remove(mSpinnerFragment).commitAllowingStateLoss();

                }
            }

            @Override
            public void onFailure(Call<List<Language>> call, Throwable t) {
                t.printStackTrace();
                // hide the spinner
                fm.beginTransaction().remove(mSpinnerFragment).commitAllowingStateLoss();
            }
        });

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        list = new ArrayList<>();
        pageCount = 1;
        dataAvailable = true;

    }

}
