package com.towkai.multimedia.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Filter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.app.VerticalGridSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.VerticalGridPresenter;

import com.towkai.multimedia.Config;
import com.towkai.multimedia.NetworkInst;
import com.towkai.multimedia.R;
import com.towkai.multimedia.model.FIlterClass;
import com.towkai.multimedia.model.Genre;
import com.towkai.multimedia.model.Language;
import com.towkai.multimedia.network.RetrofitClient;
import com.towkai.multimedia.network.api.LanguageApi;
import com.towkai.multimedia.ui.activity.ErrorActivity;
import com.towkai.multimedia.ui.activity.ItemLanguageActivity;
import com.towkai.multimedia.ui.activity.ItemMoviesActivity;
import com.towkai.multimedia.ui.activity.LeanbackActivity;
import com.towkai.multimedia.ui.presenter.LanguageCardPresenter;
import com.towkai.multimedia.ui.presenter.MoviesFilterCardPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class MoviesFilterFragment extends VerticalGridSupportFragment {
    public static final String LANGUAGE = "moviesfilter";
    private static final String TAG = MoviesFilterFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 4;
    private int pageCount = 1;
    private boolean dataAvailable = true;

    //private BackgroundHelper bgHelper;
    //private List<Genre> genres = new ArrayList<>();
    private ArrayObjectAdapter mAdapter;
    private LeanbackActivity activity;
    private ArrayList<FIlterClass> list = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

//

        activity = (LeanbackActivity) getActivity();
        activity.hideLogo();
        setTitle(getResources().getString(R.string.filter_movies));
        activity.hideDropDown();
        //bgHelper = new BackgroundHelper(getActivity());

        setOnItemViewClickedListener(getDefaultItemViewClickedListener());
        setOnItemViewSelectedListener(getDefaultItemSelectedListener());

        // setup
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

       // mAdapter = new ArrayObjectAdapter(new GenreCardPresenter());
        mAdapter = new ArrayObjectAdapter(new MoviesFilterCardPresenter());
        setAdapter(mAdapter);

        fetchGenreData(pageCount);

    }

    // click listener
    private OnItemViewClickedListener getDefaultItemViewClickedListener() {
        return new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder2, Row row) {
                FIlterClass language = (FIlterClass) o;
                Intent intent = new Intent(getActivity(), ItemMoviesActivity.class);
                intent.putExtra("id", language.getIso());
                intent.putExtra("title", language.getName());
                startActivity(intent);

            }
        };
    }


    // selected listener for setting blur background each time when the item will select.
    protected OnItemViewSelectedListener getDefaultItemSelectedListener() {
        return new OnItemViewSelectedListener() {
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, final Object item,
                                       RowPresenter.ViewHolder rowViewHolder, Row row) {


                if (item instanceof Genre) {
                    /*bgHelper = new BackgroundHelper(getActivity());
                    bgHelper.prepareBackgroundManager();
                    bgHelper.startBackgroundTimer(((Genre) item).getImageUrl());*/


                }
            }
        };
    }

    public void fetchGenreData(int pageCount) {

        if (!new NetworkInst(activity).isNetworkAvailable()) {
            Intent intent = new Intent(activity, ErrorActivity.class);
            startActivity(intent);
            activity.finish();
            return;
        }

        final SpinnerFragment mSpinnerFragment = new SpinnerFragment();
        final FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.custom_frame_layout, mSpinnerFragment).commit();
        list.add(new FIlterClass("1","Continue Watching","continue"));
        list.add(new FIlterClass("1","Most Watched Today","today"));
        list.add(new FIlterClass("2","Most Watched This Week","weekly"));
        list.add(new FIlterClass("2","Recently Released","recently"));
        list.add(new FIlterClass("2","Recently Watched","watch"));
        list.add(new FIlterClass("2","Recently Added","added"));
        list.add(new FIlterClass("2","Sort By Release date","release"));
        list.add(new FIlterClass("3","Sort By Alphabet(A->Z)","asc"));
        list.add(new FIlterClass("4","Sort By Alphabet(Z->A)","desc"));

        for (FIlterClass lan : list) {
            mAdapter.add(lan);
        }


        mAdapter.notifyArrayItemRangeChanged(list.size() - 1, list.size());
        // hide the spinner
        fm.beginTransaction().remove(mSpinnerFragment).commitAllowingStateLoss();


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        list = new ArrayList<>();
        pageCount = 1;
        dataAvailable = true;

    }

}
