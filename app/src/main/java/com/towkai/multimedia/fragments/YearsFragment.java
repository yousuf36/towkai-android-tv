package com.towkai.multimedia.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.app.VerticalGridSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.VerticalGridPresenter;

import com.towkai.multimedia.Config;
import com.towkai.multimedia.NetworkInst;
import com.towkai.multimedia.R;
import com.towkai.multimedia.model.Genre;
import com.towkai.multimedia.model.Language;
import com.towkai.multimedia.model.Year;
import com.towkai.multimedia.network.RetrofitClient;
import com.towkai.multimedia.network.api.GenreApi;
import com.towkai.multimedia.network.api.YearApi;
import com.towkai.multimedia.ui.activity.ErrorActivity;
import com.towkai.multimedia.ui.activity.ItemGenreActivity;
import com.towkai.multimedia.ui.activity.ItemYearActivity;
import com.towkai.multimedia.ui.activity.LeanbackActivity;
import com.towkai.multimedia.ui.presenter.LanguageCardPresenter;
import com.towkai.multimedia.ui.presenter.YearCardPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class YearsFragment extends VerticalGridSupportFragment {
    public static final String LANGUAGE = "language";
    private static final String TAG = YearsFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 5;
    private int pageCount = 1;
    private boolean dataAvailable = true;

    //private BackgroundHelper bgHelper;
    //private List<Genre> genres = new ArrayList<>();
    private ArrayObjectAdapter mAdapter;
    private LeanbackActivity activity;
    private ArrayList<Year> list = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);


//        list.add(new Year("1","2020-2021",""));
//        list.add(new Year("2","2018-2020",""));
//        list.add(new Year("3","2015-2018",""));
//        list.add(new Year("4","2010-2015 ",""));
//
//

        activity = (LeanbackActivity) getActivity();
        activity.hideLogo();
        setTitle(getResources().getString(R.string.years));
        activity.hideDropDown();
        //bgHelper = new BackgroundHelper(getActivity());

        setOnItemViewClickedListener(getDefaultItemViewClickedListener());
        setOnItemViewSelectedListener(getDefaultItemSelectedListener());

        // setup
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

       // mAdapter = new ArrayObjectAdapter(new GenreCardPresenter());
        mAdapter = new ArrayObjectAdapter(new YearCardPresenter());
        setAdapter(mAdapter);

        fetchGenreData(pageCount);

    }

    // click listener
    private OnItemViewClickedListener getDefaultItemViewClickedListener() {
        return new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder2, Row row) {
                Year year = (Year) o;
                Intent intent = new Intent(getActivity(), ItemYearActivity.class);
                intent.putExtra("id", year.getName());
                intent.putExtra("title", year.getName());
                startActivity(intent);

            }
        };
    }


    // selected listener for setting blur background each time when the item will select.
    protected OnItemViewSelectedListener getDefaultItemSelectedListener() {
        return new OnItemViewSelectedListener() {
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, final Object item,
                                       RowPresenter.ViewHolder rowViewHolder, Row row) {


                if (item instanceof Genre) {
                    /*bgHelper = new BackgroundHelper(getActivity());
                    bgHelper.prepareBackgroundManager();
                    bgHelper.startBackgroundTimer(((Genre) item).getImageUrl());*/


                }
            }
        };
    }

    public void fetchGenreData(int pageCount) {

//        final SpinnerFragment mSpinnerFragment = new SpinnerFragment();
//        final FragmentManager fm = getFragmentManager();
//        fm.beginTransaction().add(R.id.custom_frame_layout, mSpinnerFragment).commit();

//        for (Year year : list) {
//            mAdapter.add(year);
//        }
//
//        mAdapter.notifyArrayItemRangeChanged(list.size() - 1, list.size() );
//       // list.addAll(genreList);

//        // hide the spinner
//        fm.beginTransaction().remove(mSpinnerFragment).commitAllowingStateLoss();



        if (!new NetworkInst(activity).isNetworkAvailable()) {
            Intent intent = new Intent(activity, ErrorActivity.class);
            startActivity(intent);
            activity.finish();
            return;
        }

        final SpinnerFragment mSpinnerFragment = new SpinnerFragment();
        final FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.custom_frame_layout, mSpinnerFragment).commit();

        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        YearApi api = retrofit.create(YearApi.class);
        Call<List<String>> call = api.getALlYear(Config.API_KEY, pageCount);
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.code() == 200) {
                    List<String> yearList = response.body();
                    if (yearList.size() <= 0) {
                        dataAvailable = false;
                        Toast.makeText(activity, getResources().getString(R.string.no_data_found), Toast.LENGTH_SHORT).show();
                    }

                    for (String str : yearList) {
                        mAdapter.add(new Year("1",str,""));
                        list.add(new Year("1",str,""));
                    }

                    mAdapter.notifyArrayItemRangeChanged(yearList.size() - 1, yearList.size());
                    // hide the spinner
                    fm.beginTransaction().remove(mSpinnerFragment).commitAllowingStateLoss();

                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                t.printStackTrace();
                // hide the spinner
                fm.beginTransaction().remove(mSpinnerFragment).commitAllowingStateLoss();
            }
        });

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        list = new ArrayList<>();
        pageCount = 1;
        dataAvailable = true;

    }

}
