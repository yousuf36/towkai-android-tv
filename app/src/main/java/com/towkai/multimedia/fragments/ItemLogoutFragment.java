package com.towkai.multimedia.fragments;


import static android.content.Context.MODE_PRIVATE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.leanback.app.VerticalGridSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.VerticalGridPresenter;

import com.towkai.multimedia.Config;
import com.towkai.multimedia.Constants;
import com.towkai.multimedia.database.DatabaseHelper;
import com.towkai.multimedia.model.Movie;
import com.towkai.multimedia.network.RetrofitClient;
import com.towkai.multimedia.network.api.MovieApi;
import com.towkai.multimedia.ui.activity.ItemLogoutActivity;
import com.towkai.multimedia.ui.activity.ItemYearActivity;
import com.towkai.multimedia.ui.activity.LoginChooserActivity;
import com.towkai.multimedia.ui.activity.VideoDetailsActivity;
import com.towkai.multimedia.ui.presenter.VerticalCardPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class ItemLogoutFragment extends VerticalGridSupportFragment {

    private static final String TAG = ItemLogoutFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 8;
    private List<Movie> movies = new ArrayList<>();
    private ArrayObjectAdapter mAdapter;
    //private BackgroundHelper bgHelper;
    public static final String MOVIE = "movie";
    private int pageCount = 1;
    private boolean dataAvailable = true;


    private Context mContext;
    private String title;
    private String id = "";
    private ItemLogoutActivity activity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();
        title = getActivity().getIntent().getStringExtra("title");
        id = getActivity().getIntent().getStringExtra("id");
        activity = (ItemLogoutActivity) getActivity();

        setTitle(title);
        //bgHelper = new BackgroundHelper(getActivity());

        setOnItemViewClickedListener(getDefaultItemViewClickedListener());
        setOnItemViewSelectedListener(getDefaultItemSelectedListener());

        setupFragment();
    }

    private void setupFragment() {
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);
        // mAdapter = new ArrayObjectAdapter(new CardPresenter());
        mAdapter = new ArrayObjectAdapter(new VerticalCardPresenter(MOVIE));
        setAdapter(mAdapter);

        fetchMovieData(id, pageCount);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("Logout Confirmation")
                .setMessage("Are you sure you want to logout?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences.Editor preferences = getActivity().getSharedPreferences(Constants.USER_LOGIN_STATUS, MODE_PRIVATE).edit();
                        preferences.putBoolean(Constants.USER_LOGIN_STATUS, false);
                        preferences.apply();
                        preferences.commit();
                        DatabaseHelper db = new DatabaseHelper(getActivity());
                        db.deleteUserData();
                        Intent i = new Intent(getActivity(), LoginChooserActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        activity.finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert);
        final AlertDialog dialog = builder.create();
        dialog.show();


    }

    private void fetchMovieData(String id, int pageCount) {
//
//        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
//        MovieApi api = retrofit.create(MovieApi.class);
//        Call<List<Movie>> call = api.getMovieByYear(Config.API_KEY, id, pageCount);
//        call.enqueue(new Callback<List<Movie>>() {
//            @Override
//            public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
//                if (response.code() == 200) {
//                    List<Movie> movieList = response.body();
//                    if (movieList.size() <= 0) {
//                        dataAvailable = false;
//                        //Toast.makeText(activity, getResources().getString(R.string.no_data_found), Toast.LENGTH_SHORT).show();
//                    }
//
////                    if (movies.size() > 0 && movies.size() == movieList.size())
////                        return;
//                    for (Movie movie : movieList) {
//                        mAdapter.add(movie);
//                    }
//
//                    mAdapter.notifyArrayItemRangeChanged(movieList.size() - 1, movieList.size() + movies.size());
//                    movies.addAll(movieList);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Movie>> call, Throwable t) {
//                Log.e("Genre Item", "code: " + t.getLocalizedMessage());
//            }
//        });
    }

    // click listener
    private OnItemViewClickedListener getDefaultItemViewClickedListener() {
        return new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder viewHolder, Object o,
                                      RowPresenter.ViewHolder viewHolder2, Row row) {
//
//                Movie movie = (Movie) o;
//
//                Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
//                intent.putExtra("id", movie.getVideosId());
//                intent.putExtra("type", "movie");
//                intent.putExtra("thumbImage", movie.getThumbnailUrl());
//                startActivity(intent);
            }
        };
    }


    // selected listener for setting blur background each time when the item will select.
    protected OnItemViewSelectedListener getDefaultItemSelectedListener() {
        return new OnItemViewSelectedListener() {
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, final Object item,
                                       RowPresenter.ViewHolder rowViewHolder, Row row) {

                // pagination
//                if (dataAvailable) {
//                    int itemPos = mAdapter.indexOf(item);
//                  //  if (itemPos == movies.size() - 1) {
//                        if (itemPos >= movies.size() - 10) {
//                        pageCount++;
//                        fetchMovieData(id, pageCount);
//                    }
//                }
//
//                //Log.d("iamge url: ------------------------------", itemPos+" : "+ movies.size());
//                // change the background color when the item will select
//                if (item instanceof Movie) {
//                    /*bgHelper = new BackgroundHelper(getActivity());
//                    bgHelper.prepareBackgroundManager();
//                    bgHelper.startBackgroundTimer(((Movie) item).getThumbnailUrl());*/
//
//                }

            }
        };
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        movies = new ArrayList<>();
        pageCount = 1;
        dataAvailable = true;

    }

}
