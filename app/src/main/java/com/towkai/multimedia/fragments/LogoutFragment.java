package com.towkai.multimedia.fragments;


import static android.content.Context.MODE_PRIVATE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.app.VerticalGridSupportFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.VerticalGridPresenter;

import com.towkai.multimedia.Constants;
import com.towkai.multimedia.NetworkInst;
import com.towkai.multimedia.R;
import com.towkai.multimedia.database.DatabaseHelper;
import com.towkai.multimedia.model.FIlterClass;
import com.towkai.multimedia.model.Genre;
import com.towkai.multimedia.ui.activity.ErrorActivity;
import com.towkai.multimedia.ui.activity.ItemLogoutActivity;
import com.towkai.multimedia.ui.activity.ItemMoviesActivity;
import com.towkai.multimedia.ui.activity.LeanbackActivity;
import com.towkai.multimedia.ui.activity.LoginChooserActivity;
import com.towkai.multimedia.ui.presenter.LogoutCardPresenter;
import com.towkai.multimedia.ui.presenter.MoviesFilterCardPresenter;

import java.util.ArrayList;


public class LogoutFragment extends VerticalGridSupportFragment {
    public static final String LANGUAGE = "moviesfilter";
    private static final String TAG = LogoutFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 4;
    private int pageCount = 1;
    private boolean dataAvailable = true;

    //private BackgroundHelper bgHelper;
    //private List<Genre> genres = new ArrayList<>();
    private ArrayObjectAdapter mAdapter;
    private LeanbackActivity activity;
    private ArrayList<FIlterClass> list = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

//

        activity = (LeanbackActivity) getActivity();
        activity.hideLogo();
        setTitle(getResources().getString(R.string.log_out));
        activity.hideDropDown();
        //bgHelper = new BackgroundHelper(getActivity());

        setOnItemViewClickedListener(getDefaultItemViewClickedListener());
        setOnItemViewSelectedListener(getDefaultItemSelectedListener());

        // setup
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

       // mAdapter = new ArrayObjectAdapter(new GenreCardPresenter());
        mAdapter = new ArrayObjectAdapter(new LogoutCardPresenter());
        setAdapter(mAdapter);

        fetchGenreData(pageCount);

    }

    // click listener
    private OnItemViewClickedListener getDefaultItemViewClickedListener() {
        return new OnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder viewHolder, Object o, RowPresenter.ViewHolder viewHolder2, Row row) {
                FIlterClass language = (FIlterClass) o;
                Intent intent = new Intent(getActivity(), ItemLogoutActivity.class);
                intent.putExtra("id", language.getIso());
                intent.putExtra("title", language.getName());
                startActivity(intent);

//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
//                        .setTitle("Logout Confirmation")
//                        .setMessage("Are you sure you want to logout?")
//
//                        // Specifying a listener allows you to take an action before dismissing the dialog.
//                        // The dialog is automatically dismissed when a dialog button is clicked.
//                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//
//                                SharedPreferences.Editor preferences = getActivity().getSharedPreferences(Constants.USER_LOGIN_STATUS, MODE_PRIVATE).edit();
//                                preferences.putBoolean(Constants.USER_LOGIN_STATUS, false);
//                                preferences.apply();
//                                preferences.commit();
//                                DatabaseHelper db = new DatabaseHelper(getActivity());
//                                db.deleteUserData();
//                                Intent i = new Intent(getActivity(), LoginChooserActivity.class);
//                                // set the new task and clear flags
//                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(i);
//                            }
//                        })
//
//                        // A null listener allows the button to dismiss the dialog and take no further action.
//                        .setNegativeButton(android.R.string.no, null)
//                        .setIcon(android.R.drawable.ic_dialog_alert);
//                final AlertDialog dialog = builder.create();
//                dialog.show();


            }
        };
    }


    // selected listener for setting blur background each time when the item will select.
    protected OnItemViewSelectedListener getDefaultItemSelectedListener() {
        return new OnItemViewSelectedListener() {
            public void onItemSelected(Presenter.ViewHolder itemViewHolder, final Object item,
                                       RowPresenter.ViewHolder rowViewHolder, Row row) {


                if (item instanceof Genre) {
                    /*bgHelper = new BackgroundHelper(getActivity());
                    bgHelper.prepareBackgroundManager();
                    bgHelper.startBackgroundTimer(((Genre) item).getImageUrl());*/


                }
            }
        };
    }

    public void fetchGenreData(int pageCount) {

        if (!new NetworkInst(activity).isNetworkAvailable()) {
            Intent intent = new Intent(activity, ErrorActivity.class);
            startActivity(intent);
            activity.finish();
            return;
        }

        final SpinnerFragment mSpinnerFragment = new SpinnerFragment();
        final FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.custom_frame_layout, mSpinnerFragment).commit();
        list.add(new FIlterClass("1",""+getResources().getString(R.string.log_out),""));
        for (FIlterClass lan : list) {
            mAdapter.add(lan);
        }
        mAdapter.notifyArrayItemRangeChanged(list.size() - 1, list.size());
        // hide the spinner
        fm.beginTransaction().remove(mSpinnerFragment).commitAllowingStateLoss();


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        list = new ArrayList<>();
        pageCount = 1;
        dataAvailable = true;

    }

}
