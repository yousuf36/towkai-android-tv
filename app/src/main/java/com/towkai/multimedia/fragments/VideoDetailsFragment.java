package com.towkai.multimedia.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.app.DetailsSupportFragment;
import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.DetailsOverviewLogoPresenter;
import androidx.leanback.widget.DetailsOverviewRow;
import androidx.leanback.widget.FullWidthDetailsOverviewRowPresenter;
import androidx.leanback.widget.FullWidthDetailsOverviewSharedElementHelper;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.ListRowPresenter;
import androidx.leanback.widget.OnActionClickedListener;
import androidx.leanback.widget.SparseArrayObjectAdapter;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.towkai.multimedia.Config;
import com.towkai.multimedia.model.SinglePlayBack;
import com.towkai.multimedia.ui.activity.PlayerActivity;
import com.towkai.multimedia.ui.activity.VideoDetailsActivity;
import com.towkai.multimedia.utils.PreferenceUtils;
import com.towkai.multimedia.R;
import com.towkai.multimedia.adapter.ServerAdapter;
import com.towkai.multimedia.database.DatabaseHelper;
import com.towkai.multimedia.model.Episode;
import com.towkai.multimedia.model.FavoriteModel;
import com.towkai.multimedia.model.MovieSingleDetails;
import com.towkai.multimedia.model.RelatedMovie;
import com.towkai.multimedia.model.Season;
import com.towkai.multimedia.model.Video;
import com.towkai.multimedia.network.RetrofitClient;
import com.towkai.multimedia.network.api.DetailsApi;
import com.towkai.multimedia.network.api.FavoriteApi;
import com.towkai.multimedia.ui.BackgroundHelper;
import com.towkai.multimedia.ui.PaletteColors;
import com.towkai.multimedia.ui.Utils;

import com.towkai.multimedia.ui.presenter.ActionButtonPresenter;
import com.towkai.multimedia.ui.presenter.CustomMovieDetailsPresenter;
import com.towkai.multimedia.ui.presenter.EpisodPresenter;
import com.towkai.multimedia.ui.presenter.MovieDetailsDescriptionPresenter;
import com.towkai.multimedia.ui.presenter.RelatedPresenter;
import com.towkai.multimedia.utils.PaidDialog;
import com.towkai.multimedia.utils.ToastMsg;
import com.towkai.multimedia.video_service.PlaybackModel;
import com.towkai.multimedia.video_service.VideoPlaybackActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class VideoDetailsFragment extends DetailsSupportFragment implements Palette.PaletteAsyncListener {
    public static String TRANSITION_NAME = "poster_transition";
    private FullWidthDetailsOverviewRowPresenter mFullWidthMovieDetailsPresenter;
    private ArrayObjectAdapter mAdapter;

    private DetailsOverviewRow mDetailsOverviewRow;
    public static MovieSingleDetails movieDetails = null;
    private Context mContext;
    private String type;
    private String id;
    private String thumbUrl;
    private BackgroundHelper bgHelper;

    private static final int ACTION_PLAY = 1;
    private static final int ACTION_WATCH_LATER = 2;
    private String userId = "";
    private String isPaid = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();

        type = getActivity().getIntent().getStringExtra("type");
        id = getActivity().getIntent().getStringExtra("id");
        thumbUrl = getActivity().getIntent().getStringExtra("thumbImage");

        //get userId from DB
        this.userId = PreferenceUtils.getUserId(getContext());


        bgHelper = new BackgroundHelper(getActivity());
        bgHelper.prepareBackgroundManager();
        bgHelper.updateBackground(thumbUrl);


    }


    @Override
    public void onStart() {
        super.onStart();

        setUpAdapter();
        setUpDetailsOverviewRow();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setUpAdapter() {
        // Create the FullWidthPresenter
        mFullWidthMovieDetailsPresenter = new CustomMovieDetailsPresenter(new MovieDetailsDescriptionPresenter(),
                new DetailsOverviewLogoPresenter());

        //mFullWidthMovieDetailsPresenter.setActionsBackgroundColor(ContextCompat.getColor(getActivity(), R.color.black_30));
        mFullWidthMovieDetailsPresenter.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.black_50));

        // Handle the transition, the Helper is mainly used because the ActivityTransition is being passed from
        // The Activity into the Fragment
        FullWidthDetailsOverviewSharedElementHelper helper = new FullWidthDetailsOverviewSharedElementHelper();
        helper.setSharedElementEnterTransition(getActivity(), TRANSITION_NAME); // the transition name is important
        mFullWidthMovieDetailsPresenter.setListener(helper); // Attach the listener
        // Define if this element is participating in the transition or not
        mFullWidthMovieDetailsPresenter.setParticipatingEntranceTransition(false);

        // Class presenter selector allows the Adapter to render Rows and the details
        // It can be used in any of the Adapters by the Leanback library
        ClassPresenterSelector classPresenterSelector = new ClassPresenterSelector();
        classPresenterSelector.addClassPresenter(DetailsOverviewRow.class, mFullWidthMovieDetailsPresenter);
        classPresenterSelector.addClassPresenter(ListRow.class, new ListRowPresenter());
        mAdapter = new ArrayObjectAdapter(classPresenterSelector);

        // Sets the adapter to the fragment
        setAdapter(mAdapter);

    }

    private void setUpDetailsOverviewRow() {
        mDetailsOverviewRow = new DetailsOverviewRow(new MovieSingleDetails());
        mAdapter.add(mDetailsOverviewRow);
        loadImage(thumbUrl);

        if (type.equals("movie")) {
            // fetch movie details
            getData(type, id);
            //getFavStatus();

        } else if (type.equals("tvseries")) {
            // fetch tv series details
            getTvSeries(type, id);
            // getFavStatus();
        }
    }

    public void setActionAdapter() {
        if (type.equals("movie")) {
            setMovieActionAdapter();
        } else if (type.equals("tvseries")) {
            //setTvSeriesActionAdapter(favAdded);
        }

    }

    public void setMovieActionAdapter() {
        final SparseArrayObjectAdapter adapter = new SparseArrayObjectAdapter(new ActionButtonPresenter());

        //set play button text
        //if user has subscription, button text will be "play now"
        DatabaseHelper db = new DatabaseHelper(getContext());
        final String status = db.getActiveStatusData().getStatus();

        Log.e("DetailsScreenAction", "setMovieActionAdapter: " + movieDetails.getVideos().size());

        if (isPaid.equals("1")) {
            if (status.equals("active")) {
                if (PreferenceUtils.isValid(getActivity())) {
                    adapter.set(ACTION_PLAY, new Action(ACTION_PLAY, getResources().getString(R.string.play_now)));
                } else {
                    adapter.set(ACTION_PLAY, new Action(ACTION_PLAY, getResources().getString(R.string.go_premium)));
                }
            } else {
                adapter.set(ACTION_PLAY, new Action(ACTION_PLAY, getResources().getString(R.string.go_premium)));
            }
        } else {
            adapter.set(ACTION_PLAY, new Action(ACTION_PLAY, getResources().getString(R.string.play_now)));
        }

        mDetailsOverviewRow.setActionsAdapter(adapter);

        mFullWidthMovieDetailsPresenter.setOnActionClickedListener(new OnActionClickedListener() {
            @Override
            public void onActionClicked(Action action) {

                Log.e("videodetails", "video details action.getId() " + action.getId() + " movieDetails.getIsPaid() " +
                        movieDetails.getIsPaid() + " status " + status + " ");

                if (action.getId() == 1) {
                    if (movieDetails != null) {
                        if (movieDetails.getIsPaid().equals("1")) {
                            if (status.equals("active")) {
                                if (PreferenceUtils.isValid(getActivity())) {
                                    openServerDialog(movieDetails.getVideos());
                                } else {
                                    //saved data is not valid, because it was saved more than 2 hours ago
                                    PreferenceUtils.updateSubscriptionStatus(getActivity());
                                }
                            } else {
                                //subscription is not active
                                //new PaidDialog(getActivity()).showPaidContentAlertDialog();
                                PaidDialog dialog = new PaidDialog(getContext());
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                                dialog.show();
                            }
                        } else {
                            openServerDialog(movieDetails.getVideos());
                        }
                    } else {
                        Log.e("DetailsScreenAction", "Movie Details null");
                    }
                } /*else {

                    if (favStatus) {
                        // remove from fav
                        removeFromFav();
                    } else {
                        // add to fav
                        addToFav();
                    }

                }*/
            }
        });
    }

    public void setTvSeriesActionAdapter(boolean favAdded) {
        SparseArrayObjectAdapter adapter = new SparseArrayObjectAdapter(new ActionButtonPresenter());
        if (favAdded) {
            adapter.set(ACTION_WATCH_LATER, new Action(ACTION_WATCH_LATER, getResources().getString(R.string.remove_from_fav)));
        } else {
            adapter.set(ACTION_WATCH_LATER, new Action(ACTION_WATCH_LATER, getResources().getString(R.string.add_to_fav)));
        }

        mDetailsOverviewRow.setActionsAdapter(adapter);

        mFullWidthMovieDetailsPresenter.setOnActionClickedListener(new OnActionClickedListener() {
            @Override
            public void onActionClicked(Action action) {
                /*if (favStatus) {
                    // remove from fav
                    removeFromFav();
                } else {
                    // add to fav
                    addToFav();
                }*/
            }
        });
    }

    private void loadImage(String url) {
        /*mDetailsOverviewRow.setImageDrawable(getContext().getResources().
                getDrawable(R.drawable.logo));*/
        Picasso.get()
                .load(url)
                .resize(300, 500)
                .centerCrop()
                .placeholder(R.drawable.poster_placeholder)
                .into(new PicassoImageCardViewTarget());

    }

    private void bindMovieDetails(MovieSingleDetails singleDetails) {
        movieDetails = singleDetails;
        //   Log.e("DetailsScreenAction", "bindMovieDetails: " + movieDetails.getVideos().size());
        // Bind the details to the row
        mDetailsOverviewRow.setItem(movieDetails);
        loadImage(thumbUrl);
    }

    private void changePalette(Bitmap bmp) {
        Palette.from(bmp).generate(this);
    }

    @Override
    public void onGenerated(Palette palette) {
        PaletteColors colors = Utils.getPaletteColors(palette);
        mFullWidthMovieDetailsPresenter.setActionsBackgroundColor(colors.getStatusBarColor());
        mFullWidthMovieDetailsPresenter.setBackgroundColor(colors.getToolbarBackgroundColor());
    }

    class PicassoImageCardViewTarget implements Target {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            mDetailsOverviewRow.setImageBitmap(getContext(), bitmap);
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }


        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    }

    public static int dpToPx(int dp, Context ctx) {
        float density = ctx.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public void openServerDialog(final List<Video> videos) {
//.........................................hide alert dialog..........................................................

        if (videos.get(0).getFileUrl() != null && videos.get(0).getFileUrl().length() > 0) {
            PlaybackModel video = new PlaybackModel();
            video.setId(Long.parseLong(id));
            video.setTitle(movieDetails.getTitle());
            video.setDescription(movieDetails.getDescription());
            video.setCategory("movie");
            video.setVideo(videos.get(0));
            ArrayList<Video> videoListForIntent = new ArrayList<>(videos);
            video.setVideoList(videoListForIntent);
            video.setVideoUrl(videos.get(0).getFileUrl());
            video.setVideoType(videos.get(0).getFileType());
            video.setBgImageUrl(movieDetails.getPosterUrl());
            video.setCardImageUrl(movieDetails.getThumbnailUrl());
            video.setIsPaid(movieDetails.getIsPaid());

            Log.e("video details", ",,,,,,,,,,,,,,video getWatchTime  " + movieDetails.getWatchTime());


            Intent playerIntent = new Intent(getActivity(), PlayerActivity.class);
            playerIntent.putExtra(VideoPlaybackActivity.EXTRA_VIDEO, video);
            if (movieDetails.getWatchTime() != null && movieDetails.getWatchTime().length() > 0)
                playerIntent.putExtra(VideoPlaybackActivity.EXTRA_POSITION, Long.valueOf(movieDetails.getWatchTime()));
            startActivity(playerIntent);

        } else {
            Toast.makeText(getActivity(), "No Video Available", Toast.LENGTH_SHORT).show();
        }

//.......................................show alert dialog for playing video..................................................

//        if (videos.size() != 0) {
//            List<Video> videoList = new ArrayList<>();
//            videoList.clear();
//
//            for (Video video : videos) {
//                if (!video.getFileType().equalsIgnoreCase("embed")) {
//                    videoList.add(video);
//                }
//            }
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_server_tv, null);
//            RecyclerView serverRv = view.findViewById(R.id.serverRv);
//            ServerAdapter serverAdapter = new ServerAdapter(getActivity(), videoList, "movie");
//            serverRv.setLayoutManager(new LinearLayoutManager(getActivity()));
//            serverRv.setHasFixedSize(true);
//            serverRv.setAdapter(serverAdapter);
//
//            Button closeBt = view.findViewById(R.id.close_bt);
//
//            builder.setView(view);
//
//            final AlertDialog dialog = builder.create();
//            dialog.show();
//
//            closeBt.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//                }
//            });
//
//            final ServerAdapter.OriginalViewHolder[] viewHolder = {null};
//            serverAdapter.setOnItemClickListener(new ServerAdapter.OnItemClickListener() {
//
//                @Override
//                public void onItemClick(View view, Video obj, int position, ServerAdapter.OriginalViewHolder holder) {
//                    Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity())
//                            .toBundle();
//
//                    PlaybackModel video = new PlaybackModel();
//                    video.setId(Long.parseLong(id));
//                    video.setTitle(movieDetails.getTitle());
//                    video.setDescription(movieDetails.getDescription());
//                    video.setCategory("movie");
//                    video.setVideo(obj);
//                    ArrayList<Video> videoListForIntent = new ArrayList<>(videoList);
//                    video.setVideoList(videoListForIntent);
//                    video.setVideoUrl(obj.getFileUrl());
//                    video.setVideoType(obj.getFileType());
//                    video.setBgImageUrl(movieDetails.getPosterUrl());
//                    video.setCardImageUrl(movieDetails.getThumbnailUrl());
//                    video.setIsPaid(movieDetails.getIsPaid());
//
//                    Intent playerIntent = new Intent(getActivity(), PlayerActivity.class);
//                    playerIntent.putExtra(VideoPlaybackActivity.EXTRA_VIDEO, video);
//                    startActivity(playerIntent);
//                    dialog.dismiss();
//                }
//            });
//        }else {
//            new ToastMsg(getContext()).toastIconError("No video available.");
//        }

    }


    private void getData(String vtype, final String vId) {
        Log.e("DetailsScreenAction", "user id : " + PreferenceUtils.getUserId(getActivity()));
        Log.e("DetailsScreenAction", "viceo id : " + vId);
        final SpinnerFragment spinnerFragment = new SpinnerFragment();
        final FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.details_fragment, spinnerFragment).commit();

        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        DetailsApi api = retrofit.create(DetailsApi.class);
        Call<MovieSingleDetails> call = api.getSingleDetail(Config.API_KEY, vtype, vId, PreferenceUtils.getUserId(getActivity()));
        call.enqueue(new Callback<MovieSingleDetails>() {
            @Override
            public void onResponse(Call<MovieSingleDetails> call, Response<MovieSingleDetails> response) {
                if (response.code() == 200) {
                    MovieSingleDetails singleDetails = new MovieSingleDetails();
                    singleDetails = response.body();
                    singleDetails.setType("movie");
                    isPaid = response.body().getIsPaid();
                    movieDetails = singleDetails;
                    bindMovieDetails(response.body());
                    setMovieActionAdapter();
                    Log.e("DetailsScreenAction", "Server: " + singleDetails.getVideos().size());

                    String[] subcategories = {
                            "You may also like"
                    };

                    ArrayObjectAdapter rowAdapter = new ArrayObjectAdapter(new RelatedPresenter(getActivity()));
                    for (RelatedMovie model : response.body().getRelatedMovie()) {
                        model.setType("movie");
                        rowAdapter.add(model);
                    }
                    HeaderItem header = new HeaderItem(0, subcategories[0]);
                    mAdapter.add(new ListRow(header, rowAdapter));

                }
                fm.beginTransaction().remove(spinnerFragment).commitAllowingStateLoss();
            }

            @Override
            public void onFailure(Call<MovieSingleDetails> call, Throwable t) {
                fm.beginTransaction().remove(spinnerFragment).commitAllowingStateLoss();
            }
        });

    }

    private void getTvSeries(String vtype, final String vId) {

        final SpinnerFragment spinnerFragment = new SpinnerFragment();
        final FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.details_fragment, spinnerFragment).commit();

        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        DetailsApi api = retrofit.create(DetailsApi.class);
        Call<MovieSingleDetails> call = api.getSingleDetail(Config.API_KEY, vtype, vId, PreferenceUtils.getUserId(getActivity()));
        call.enqueue(new Callback<MovieSingleDetails>() {
            @Override
            public void onResponse(Call<MovieSingleDetails> call, Response<MovieSingleDetails> response) {
                if (response.code() == 200) {
                    MovieSingleDetails singleDetails = new MovieSingleDetails();
                    singleDetails = response.body();
                    //   singleDetails.
                    singleDetails.setType("tvseries");
                    isPaid = response.body().getIsPaid();
                    //setTvSeriesActionAdapter(favStatus);
                    bindMovieDetails(response.body());

                    String[] subcategories = {
                            "You may also like"
                    };

                    List<Season> seasons = new ArrayList<Season>();
                    seasons.addAll(response.body().getSeason());

                    if (seasons.size() == 0) {
                        Toast.makeText(mContext, "Seasons are not found. :(", Toast.LENGTH_SHORT).show();
                    }

                    for (int i = 0; i <= seasons.size(); i++) { // <= for related content

                        if (i == seasons.size()) {
                            // set related content
                            ArrayObjectAdapter rowAdapter = new ArrayObjectAdapter(new RelatedPresenter(getActivity()));
                            for (RelatedMovie model : response.body().getRelatedTvseries()) {
                                model.setType("tvseries");
                                rowAdapter.add(model);
                            }
                            HeaderItem header = new HeaderItem(i, subcategories[0]);
                            mAdapter.add(new ListRow(header, rowAdapter));
                        } else {
                            // set season content
                            Season season = seasons.get(i);
                            ArrayObjectAdapter rowAdapter = new ArrayObjectAdapter(new EpisodPresenter());


                            for (Episode episode : season.getEpisodes()) {
                                episode.setIsPaid(isPaid);
                                episode.setSeasonName(season.getSeasonsName());
                                episode.setSeasonId(season.getSeasonsId());
                                episode.setTvSeriesTitle(singleDetails.getTitle());
                                episode.setCardBackgroundUrl(singleDetails.getPosterUrl());
                                episode.setvId(vId);
                                Log.e("videodetails","vid : "+vId+"  episode:  "+episode.getEpisodesId());
                                rowAdapter.add(episode);
                            }
                            HeaderItem header = new HeaderItem(i, "Season: " + season.getSeasonsName());
                            mAdapter.add(new ListRow(header, rowAdapter));

                        }

                    }

                }
                fm.beginTransaction().remove(spinnerFragment).commitAllowingStateLoss();
            }

            @Override
            public void onFailure(Call<MovieSingleDetails> call, Throwable t) {
                fm.beginTransaction().remove(spinnerFragment).commitAllowingStateLoss();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        movieDetails = null;
    }
}