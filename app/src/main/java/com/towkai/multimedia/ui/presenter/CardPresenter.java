package com.towkai.multimedia.ui.presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.ImageCardView;
import androidx.leanback.widget.Presenter;

import com.towkai.multimedia.R;
import com.towkai.multimedia.model.VideoContent;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class CardPresenter extends Presenter {

    private static int CARD_WIDTH = 185;
    private static int CARD_HEIGHT = 278;


    @SuppressLint("StaticFieldLeak")
    private static Context mContext;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Log.d("onCreateViewHolder", "creating viewholder");
        mContext = parent.getContext();
        ImageCardView cardView = new ImageCardView(mContext);
      //  ImageCardView  cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_imageview, parent, false);
        cardView.setFocusable(true);
        cardView.setFocusableInTouchMode(true);
        cardView.requestLayout();
        cardView.setInfoVisibility(View.VISIBLE);

        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        VideoContent video = (VideoContent) item;
        ((ViewHolder) viewHolder).mCardView.setMainImageDimensions(CARD_WIDTH, CARD_HEIGHT);
        ((ViewHolder) viewHolder).updateCardViewImage(video.getThumbnailUrl());
        ((ViewHolder) viewHolder).mCardView.setTitleText(video.getTitle());
        ((ViewHolder) viewHolder).mCardView.setContentText(video.getVideoQuality()+"  \t\t\t\t\t\t"+video.getRelease());
        ((ViewHolder) viewHolder).updateContent(video.getThumbnailUrl());
        ((ViewHolder) viewHolder).mCardView.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);

    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }


    class ViewHolder extends Presenter.ViewHolder {

        private ImageCardView mCardView;
        private Drawable mDefaultCardImage;
        private PicassoImageCardViewTarget mImageCardViewTarget;

        @SuppressLint("UseCompatLoadingForDrawables")
        public ViewHolder(View view) {
            super(view);
            mCardView = (ImageCardView) view;
            mImageCardViewTarget = new PicassoImageCardViewTarget(mCardView);
            mDefaultCardImage = mContext
                    .getResources()
                    .getDrawable(R.drawable.logo);
        }

        public ImageCardView getCardView() {
            return mCardView;
        }

        protected void updateCardViewImage(String url) {

            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.poster_placeholder)
                    .error(mDefaultCardImage)
                    .into(mCardView.getMainImageView());
        }
        protected void updateContent(String url) {
Log.e("cardpresenter","Card Presenter .........................................."+mCardView.getTitleText());
                 //  mCardView.setTitleText(url);
        }
    }


    static class PicassoImageCardViewTarget implements Target {


        private ImageCardView mImageCardView;

        public PicassoImageCardViewTarget(ImageCardView mImageCardView) {
            this.mImageCardView = mImageCardView;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Drawable bitmapDrawable = new BitmapDrawable(mContext.getResources(), bitmap);
            mImageCardView.setMainImage(bitmapDrawable);
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
            mImageCardView.setMainImage(errorDrawable);
        }


        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    }

}





//
//public class CardPresenter extends Presenter {
//
//    private static int CARD_WIDTH = 185;
//    private static int CARD_HEIGHT = 278;
//
//
//    @SuppressLint("StaticFieldLeak")
//    private static Context mContext;
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent) {
//        Log.d("onCreateViewHolder", "creating viewholder");
//        mContext = parent.getContext();
//        // ImageCardView cardView = new ImageCardView(mContext);
//        View  cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_imageview, parent, false);
//
//        cardView.setFocusable(true);
//        cardView.setFocusableInTouchMode(true);
//        cardView.requestLayout();
////        cardView.setInfoVisibility(View.VISIBLE);
//
//        return new ViewHolder(cardView);
//    }
//
//    @Override
//    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
//        VideoContent video = (VideoContent) item;
//        //  ((ViewHolder) viewHolder).mCardView.setMainImageDimensions(CARD_WIDTH, CARD_HEIGHT);
//        ((ViewHolder) viewHolder).updateCardViewImage(video.getThumbnailUrl());
//        ((ViewHolder) viewHolder).tvTitle.setText((video.getTitle()));
//
////        ((ViewHolder) viewHolder).mCardView.setContentText(video.getDescription());
////        ((ViewHolder) viewHolder).updateContent(video.getThumbnailUrl());
////        ((ViewHolder) viewHolder).mCardView.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
//
//    }
//
//    @Override
//    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
//
//    }
//
//
//    public static class ViewHolder extends Presenter.ViewHolder {
//
//
//        ImageView imageView;
//        TextView tvTitle;
//        private View mCardView;
//        private Drawable mDefaultCardImage;
//        private PicassoImageCardViewTarget mImageCardViewTarget;
//
//        @SuppressLint("UseCompatLoadingForDrawables")
//        public ViewHolder(View view) {
//            super(view);
//            mCardView = view;
//            imageView=view.findViewById(R.id.imageView);
//            tvTitle=view.findViewById(R.id.tvTitle);
////            mImageCardViewTarget = new PicassoImageCardViewTarget(mCardView);
//            mDefaultCardImage = mContext
//                    .getResources()
//                    .getDrawable(R.drawable.logo);
//        }
//
//        public View getCardView() {
//            return mCardView;
//        }
//
//        protected void updateCardViewImage(String url) {
//
//            Picasso.get()
//                    .load(url)
//                    .placeholder(R.drawable.poster_placeholder)
//                    .error(mDefaultCardImage)
//                    .into(imageView);
//        }
//        protected void updateContent(String url) {
//            //Log.e("cardpresenter","Card Presenter .........................................."+mCardView.getTitleText());
//            //  mCardView.setTitleText(url);
//        }
//    }
//
//
//    static class PicassoImageCardViewTarget implements Target {
//
//
//        private ImageCardView mImageCardView;
//
//        public PicassoImageCardViewTarget(ImageCardView mImageCardView) {
//            this.mImageCardView = mImageCardView;
//        }
//
//        @Override
//        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//            Drawable bitmapDrawable = new BitmapDrawable(mContext.getResources(), bitmap);
//            mImageCardView.setMainImage(bitmapDrawable);
//        }
//
//        @Override
//        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//            mImageCardView.setMainImage(errorDrawable);
//        }
//
//
//        @Override
//        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//        }
//    }
//
//}
