package com.towkai.multimedia.ui.activity;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.SearchOrbView;
import androidx.leanback.widget.VerticalGridView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.towkai.multimedia.Constants;
import com.towkai.multimedia.NetworkInst;
import com.towkai.multimedia.database.DatabaseHelper;
import com.towkai.multimedia.fragments.CustomRowsFragment;
import com.towkai.multimedia.fragments.LanguageFragment;
import com.towkai.multimedia.fragments.LogoutFragment;
import com.towkai.multimedia.fragments.MoviesFilterFragment;
import com.towkai.multimedia.fragments.TvSeriesFilteredFragment;
import com.towkai.multimedia.fragments.YearsFragment;
import com.towkai.multimedia.ui.Utils;
import com.towkai.multimedia.utils.PreferenceUtils;
import com.towkai.multimedia.R;
import com.towkai.multimedia.fragments.CountryFragment;
import com.towkai.multimedia.fragments.CustomHeadersFragment;
import com.towkai.multimedia.fragments.FavouriteFragment;
import com.towkai.multimedia.fragments.GenreFragment;
import com.towkai.multimedia.fragments.HomeFragment;
import com.towkai.multimedia.fragments.MoviesFragment;
import com.towkai.multimedia.fragments.MyAccountFragment;
import com.towkai.multimedia.fragments.TvSeriesFragment;
import com.towkai.multimedia.ui.CustomFrameLayout;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Locale;

public class LeanbackActivity extends FragmentActivity implements
        AdapterView.OnItemSelectedListener {
    public static final String INTENT_EXTRA_VIDEO = "intentExtraVideo";
    private CustomHeadersFragment headersFragment;
    private Fragment rowsFragment;
    private ImageView logoIv;
    private LinearLayout llHeaderTitle;
    private LinkedHashMap<Integer, Fragment> fragments;
    private SearchOrbView orbView;
    private boolean navigationDrawerOpen;
    private static final float NAVIGATION_DRAWER_SCALE_FACTOR = 0.9f;

    private CustomFrameLayout customFrameLayout;
    private boolean rowsContainerFocused;
    private Spinner spinner;
    // String[] spinnerItems = {"A->Z", "Z->A", "Sort By Released", "Recently Added", "Recently Watched", "Recently Released"};
    String[] spinnerItems = {"Most Watched Today", "Most Watched This Week", "Recently Released", "Recently Watched", "Recently Added", "Sort By Release date", "Sort By Alphabet(A->Z)", "Sort By Alphabet(Z->A)"};

    String sort;
    MutableLiveData<String> sortLiveData = new MutableLiveData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leanback);


        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerItems);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);
        findViewById(R.id.tvLogout).setOnClickListener(view -> {
            logout();
        });

        orbView = findViewById(R.id.custom_search_orb);
        logoIv = findViewById(R.id.logo);
        llHeaderTitle = findViewById(R.id.llHeaderTitle);
        orbView.setOrbColor(getResources().getColor(R.color.colorPrimary));
        orbView.bringToFront();
        orbView.setOnOrbClickedListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intent);
            }
        });


        //get subscription status and save to sharedPreference
        PreferenceUtils.updateSubscriptionStatus(this);

        fragments = new LinkedHashMap<>();

        int CATEGORIES_NUMBER = 8;
        for (int i = 0; i < CATEGORIES_NUMBER; i++) {
            if (i == 0) {
                HomeFragment fragment = new HomeFragment();

                fragments.put(i, fragment);
            } else if (i == 1) {
                MoviesFilterFragment fragment = new MoviesFilterFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("menu", i);
                fragment.setArguments(bundle);
                fragments.put(i, fragment);
            }
//            else if (i == 1) {
//                MoviesFragment fragment = new MoviesFragment();
//                Bundle bundle = new Bundle();
//                bundle.putInt("menu", i);
//                fragment.setArguments(bundle);
//                fragments.put(i, fragment);
//            }


            else if (i == 2) {
                TvSeriesFilteredFragment fragment = new TvSeriesFilteredFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("menu", i);
                fragment.setArguments(bundle);
                fragments.put(i, fragment);
            }

//            else if (i == 2) {
//                TvSeriesFragment fragment = new TvSeriesFragment();
//                Bundle bundle = new Bundle();
//                bundle.putInt("menu", i);
//                fragment.setArguments(bundle);
//                fragments.put(i, fragment);
//            }
            else if (i == 3) {
                LanguageFragment fragment = new LanguageFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("menu", i);
                fragment.setArguments(bundle);
                fragments.put(i, fragment);
            } else if (i == 4) {
                YearsFragment fragment = new YearsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("menu", i);
                fragment.setArguments(bundle);
                fragments.put(i, fragment);
            } else if (i == 5) {
                GenreFragment fragment = new GenreFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("menu", i);
                fragment.setArguments(bundle);
                fragments.put(i, fragment);
            }
            else if (i == 6) {
                CountryFragment fragment = new CountryFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("menu", i);
                fragment.setArguments(bundle);
                fragments.put(i, fragment);

            }

            else {
              //  logout();
//                CountryFragment fragment = new CountryFragment();
//                Bundle bundle = new Bundle();
//                bundle.putInt("menu", i);
//                fragment.setArguments(bundle);
//                fragments.put(i, fragment);

                LogoutFragment fragment = new LogoutFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("menu", i);
                fragment.setArguments(bundle);
                fragments.put(i, fragment);
            }
        }


        headersFragment = new CustomHeadersFragment();

        rowsFragment = fragments.get(0);
        customFrameLayout = (CustomFrameLayout) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
        setupCustomFrameLayout();

        if (new NetworkInst(this).isNetworkAvailable()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction
                    .replace(R.id.header_container, headersFragment, "CustomHeadersFragment")
                    .replace(R.id.rows_container, rowsFragment, "CustomRowsFragment");
            transaction.commit();
        } else {
            // show no internet page
            Intent intent = new Intent(this, ErrorActivity.class);
            startActivity(intent);
            finish();
        }
    }


    //Performing action onItemSelected and onNothing selected
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        // Toast.makeText(getApplicationContext(), spinnerItems[position] , Toast.LENGTH_LONG).show();

        switch (spinnerItems[position].toLowerCase(Locale.ROOT)) {

            case "logout":

                logout();
                break;
            case "sort by alphabet(a->z)":
                sort = "asc";
                sortLiveData.postValue(sort);
                break;
            case "most watched this week":
                sort = "weekly";
                sortLiveData.postValue(sort);
                break;
            case "most watched today":
                sort = "today";
                sortLiveData.postValue(sort);
                break;

            case "sort by alphabet(z->a)":
                sort = "desc";
                sortLiveData.postValue(sort);
                break;
            case "sort by release date":
                sort = "release";
                sortLiveData.postValue(sort);
                break;
            case "recently watched":
                sort = "watch";
                sortLiveData.postValue(sort);
                break;
            case "recently released":
                sort = "recently";
                sortLiveData.postValue(sort);
                break;
            case "recently added":
                sort = "added";
                sortLiveData.postValue(sort);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    public LinkedHashMap<Integer, Fragment> getFragments() {
        return fragments;
    }

    private void setupCustomFrameLayout() {
        customFrameLayout.setOnChildFocusListener(new CustomFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
                Log.e("leanbackativity", "............................onRequestFocusInDescendants");
                if (headersFragment.getView() != null && headersFragment.getView().requestFocus(direction, previouslyFocusedRect)) {
                    return true;
                }
                return rowsFragment.getView() != null && rowsFragment.getView().requestFocus(direction, previouslyFocusedRect);
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                Log.e("leanbackativity", "............................onRequestFocusInDescendants");
                int childId = child.getId();
                if (childId == R.id.rows_container) {
                    rowsContainerFocused = true;
                    toggleHeadersFragment(false);

                } else if (childId == R.id.header_container) {
                    rowsContainerFocused = false;
                    toggleHeadersFragment(true);

                }
            }
        });

        customFrameLayout.setOnFocusSearchListener(new CustomFrameLayout.OnFocusSearchListener() {
            @Override
            public View onFocusSearch(View focused, int direction) {
                if (direction == View.FOCUS_LEFT) {
                    if (isVerticalScrolling() || navigationDrawerOpen) {
                        return focused;
                    }
                    return getVerticalGridView(headersFragment);
                } else if (direction == View.FOCUS_RIGHT) {
                    if (isVerticalScrolling() || !navigationDrawerOpen) {
                        return focused;
                    }
                    return getVerticalGridView(rowsFragment);
                } else if (focused == orbView && direction == View.FOCUS_DOWN) {
                    return navigationDrawerOpen ? getVerticalGridView(headersFragment) : getVerticalGridView(rowsFragment);
                } else if (focused != orbView && orbView.getVisibility() == View.VISIBLE && direction == View.FOCUS_UP) {

                    return orbView;
                } else {
                    return null;
                }
            }
        });
    }

    public VerticalGridView getVerticalGridView(Fragment fragment) {

        try {
//            if (fragment instanceof TvSeriesFragment) {
//
//                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
//                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
//                getVerticalGridViewMethod.setAccessible(true);
//                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
//
//                return gridView;
//            }
            if (fragment instanceof TvSeriesFilteredFragment) {

                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);

                return gridView;
            } else if (fragment instanceof MoviesFilterFragment) {

                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            }

//            else if (fragment instanceof MoviesFragment) {
//
//                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
//                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
//                getVerticalGridViewMethod.setAccessible(true);
//                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
//                return gridView;
//
//            }

            else if (fragment instanceof FavouriteFragment) {

                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            } else if (fragment instanceof GenreFragment) {

                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            } else if (fragment instanceof CountryFragment) {
                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            } else if (fragment instanceof LanguageFragment) {
                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            } else if (fragment instanceof YearsFragment) {
                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            } else if (fragment instanceof MyAccountFragment) {
                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            }  else if (fragment instanceof LogoutFragment) {
                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);
                return gridView;

            } else {
                Class baseRowFragmentClass = getClassLoader().loadClass("androidx/leanback/app/BaseRowSupportFragment");
                Method getVerticalGridViewMethod = baseRowFragmentClass.getDeclaredMethod("getVerticalGridView");
                getVerticalGridViewMethod.setAccessible(true);
                VerticalGridView gridView = (VerticalGridView) getVerticalGridViewMethod.invoke(fragment, (Object[]) null);

                return gridView;
            }


        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    public synchronized void toggleHeadersFragment(final boolean doOpen) {
        boolean condition = (doOpen != isNavigationDrawerOpen());
        if (condition) {
            final View headersContainer = (View) headersFragment.getView().getParent();
            final View rowsContainer = (View) rowsFragment.getView().getParent();

            final float delta = headersContainer.getWidth() * NAVIGATION_DRAWER_SCALE_FACTOR;

            // get current margin (a previous animation might have been interrupted)
            final int currentHeadersMargin = (((ViewGroup.MarginLayoutParams) headersContainer.getLayoutParams()).leftMargin);
            final int currentRowsMargin = (((ViewGroup.MarginLayoutParams) rowsContainer.getLayoutParams()).leftMargin);

            // calculate destination
            final int headersDestination = (doOpen ? 0 : (int) (0 - delta));
            final int rowsDestination = (doOpen ? (Utils.dpToPx(300, this)) : (int) (Utils.dpToPx(300, this) - delta));

            // calculate the delta (destination - current)
            final int headersDelta = headersDestination - currentHeadersMargin;
            final int rowsDelta = rowsDestination - currentRowsMargin;

            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    ViewGroup.MarginLayoutParams headersParams = (ViewGroup.MarginLayoutParams) headersContainer.getLayoutParams();
                    headersParams.leftMargin = (int) (currentHeadersMargin + headersDelta * interpolatedTime);
                    headersContainer.setLayoutParams(headersParams);

                    ViewGroup.MarginLayoutParams rowsParams = (ViewGroup.MarginLayoutParams) rowsContainer.getLayoutParams();
                    rowsParams.leftMargin = (int) (currentRowsMargin + rowsDelta * interpolatedTime);
                    rowsContainer.setLayoutParams(rowsParams);
                }
            };

            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    navigationDrawerOpen = doOpen;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (!doOpen) {
                        if (rowsFragment instanceof CustomRowsFragment) {
                            ((CustomRowsFragment) rowsFragment).refresh();
                        }
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

            });

            animation.setDuration(200);
            ((View) rowsContainer.getParent()).startAnimation(animation);
        }
    }

    private boolean isVerticalScrolling() {
        try {
            // don't run transition
            return getVerticalGridView(headersFragment).getScrollState()
                    != HorizontalGridView.SCROLL_STATE_IDLE
                    || getVerticalGridView(rowsFragment).getScrollState()
                    != HorizontalGridView.SCROLL_STATE_IDLE;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public synchronized boolean isNavigationDrawerOpen() {
        return navigationDrawerOpen;
    }

    public void updateCurrentRowsFragment(Fragment fragment) {
        rowsFragment = fragment;
    }


    @Override
    public void onBackPressed() {

        if (rowsContainerFocused) {
            toggleHeadersFragment(true);
            rowsContainerFocused = false;
        } else {
            super.onBackPressed();
        }

    }

    public String getSortPeramitter() {
        return sort;
    }

    public MutableLiveData<String> getSortLiveData() {
        return sortLiveData;
    }


    public void hideLogo() {
        if (logoIv != null && llHeaderTitle != null) {
            logoIv.setVisibility(View.GONE);
            llHeaderTitle.setVisibility(View.VISIBLE);
        }
    }

    public void showLogo() {
        if (logoIv != null && llHeaderTitle != null) {
            logoIv.setVisibility(View.GONE);
            llHeaderTitle.setVisibility(View.VISIBLE);
        }
    }


    public void showDropDown() {
        if (spinner != null) {
            spinner.setVisibility(View.VISIBLE);

        }
    }


    public void hideDropDown() {
        if (spinner != null) {
            spinner.setVisibility(View.GONE);
        }
    }


    public void logout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LeanbackActivity.this)
                .setTitle("Logout Confirmation")
                .setMessage("Are you sure you want to logout?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences.Editor preferences = getSharedPreferences(Constants.USER_LOGIN_STATUS, MODE_PRIVATE).edit();
                        preferences.putBoolean(Constants.USER_LOGIN_STATUS, false);
                        preferences.apply();
                        preferences.commit();
                        DatabaseHelper db = new DatabaseHelper(LeanbackActivity.this);
                        db.deleteUserData();
                        Intent i = new Intent(LeanbackActivity.this, LoginChooserActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert);
        final AlertDialog dialog = builder.create();
        dialog.show();

    }


}

