package com.towkai.multimedia.ui.activity;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.towkai.multimedia.R;

public class ItemTvSeriesActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_tvseries);
    }
}
