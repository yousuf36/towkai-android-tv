package com.towkai.multimedia.ui.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.RemoteControlClient;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.CalendarContract;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.SeekBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.tvprovider.media.tv.TvContractCompat;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultControlDispatcher;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSourceFactory;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.AdaptiveMediaSourceEventListener;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.towkai.multimedia.Config;
import com.towkai.multimedia.R;
import com.towkai.multimedia.adapter.ServerAdapter;
import com.towkai.multimedia.adapter.SubtitleListAdapter;
import com.towkai.multimedia.database.DatabaseHelper;
import com.towkai.multimedia.fragments.SpinnerFragment;
import com.towkai.multimedia.model.MovieSingleDetails;
import com.towkai.multimedia.model.RelatedMovie;
import com.towkai.multimedia.model.SinglePlayBack;
import com.towkai.multimedia.model.Subtitle;
import com.towkai.multimedia.model.UpdateWatchTimeResponse;
import com.towkai.multimedia.model.Video;
import com.towkai.multimedia.network.RetrofitClient;
import com.towkai.multimedia.network.api.DetailsApi;
import com.towkai.multimedia.ui.presenter.RelatedPresenter;
import com.towkai.multimedia.utils.PreferenceUtils;
import com.towkai.multimedia.utils.ToastMsg;
import com.towkai.multimedia.video_service.MediaSessionHelper;
import com.towkai.multimedia.video_service.MockDatabase;
import com.towkai.multimedia.video_service.PlaybackModel;
import com.towkai.multimedia.video_service.Subscription;
import com.towkai.multimedia.video_service.TvUtil;
import com.towkai.multimedia.video_service.VideoPlaybackActivity;
import com.towkai.multimedia.video_service.WatchNextAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.view.View.VISIBLE;
import static java.lang.String.valueOf;

public class PlayerActivity extends Activity {
    private static final String TAG = "PlayerActivity";
    private static final String CLASS_NAME = "com.oxoo.spagreen.ui.activity.PlayerActivity";
    private PlayerView exoPlayerView;
    private SimpleExoPlayer player;
    private RelativeLayout rootLayout;
    private MediaSource mediaSource;
    private boolean isPlaying;
    private List<Video> videos = new ArrayList<>();
    private Video video = null;
    private String url = "";
    private String videoType = "";
    private String category = "";
    private int visible;
    private ImageButton serverButton, rewindButton, fastForwardButton, subtitleButton;
    private TextView movieTitleTV, movieDescriptionTV;
    private ImageView posterImageView;
    private RelativeLayout seekBarLayout;
    private TextView liveTvTextInController;
    private ProgressBar progressBar;
    private PowerManager.WakeLock wakeLock;
    private MediaSession session;

    private long mChannelId;
    private String seasonId;
    private long mStartingPosition;
    private PlaybackModel model;
    private MediaSessionHelper mediaSessionHelper;

    boolean isHideControllerAfterCertainTime = false;
    Handler handler;
    Runnable runnable;
    long lastUpdate = 0, timeCounter = 0;
    boolean isAlreadyTimerStarted = false;
    long controllerShowingTIme = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        mChannelId = getIntent().getLongExtra(VideoPlaybackActivity.EXTRA_CHANNEL_ID, -1L);
        mStartingPosition = getIntent().getLongExtra(VideoPlaybackActivity.EXTRA_POSITION, -1L);
        //mStartingPosition=120000;
        model = (PlaybackModel) getIntent().getSerializableExtra(VideoPlaybackActivity.EXTRA_VIDEO);
        seasonId = getIntent().getStringExtra(VideoPlaybackActivity.SEASON_ID);
        assert model != null;
        url = model.getVideoUrl();
        videoType = model.getVideoType();
        category = model.getCategory();
        if (model.getVideo() != null)
            video = model.getVideo();
        if (model.getCategory().equals("movie") && mChannelId > -1L && model.getIsPaid().equals("1")) {
            //Paid Content from Channel
            //check user has subscription or not
            //if not, send user to VideoDetailsActivity
            DatabaseHelper db = new DatabaseHelper(PlayerActivity.this);
            final String status = db.getActiveStatusData().getStatus();
            if (!status.equals("active") || !PreferenceUtils.isValid(PlayerActivity.this)) {
                Intent intent = new Intent(PlayerActivity.this, VideoDetailsActivity.class);
                intent.putExtra("type", model.getCategory());
                intent.putExtra("id", model.getMovieId());
                intent.putExtra("thumbImage", model.getCardImageUrl());
                startActivity(intent, null);
                finish();
            }
        }

   //     Log.e("playactiivty", "currentid .................." + model.getId());
      //  Log.e("playactiivty", "netxtid .................." + model.getWatchNextId());
//

        intiViews();
        initVideoPlayer(url, videoType);


    }

    int controllerCounter = 0;

    private void startTimer() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                if (System.currentTimeMillis() > controllerShowingTIme + 5000) {
                    if (exoPlayerView.isControllerVisible()) {
                        hideController();
                    }
                }


//                if (exoPlayerView != null && player != null && player.getCurrentPosition() > 5000 && isHideControllerAfterCertainTime == false) {
//
//                    exoPlayerView.hideController();
//                    isHideControllerAfterCertainTime = true;
//                }

//                if (exoPlayerView.isControllerVisible() && controllerCounter >= 5) {
//                    controllerCounter = 0;
//                    exoPlayerView.hideController();
//
//                } else if (exoPlayerView.isControllerVisible()) {
//                    controllerCounter += 1;
//                } else {
//                    controllerCounter = 0;
//                }


//                if (timeCounter > 30) {
//
//                    if (player != null)
//                        Log.e("RemoteKey", "progress ........................... " + player.getCurrentPosition());
//                    Log.e("RemoteKey", "timeCounter ........................... " + timeCounter);
//                    timeCounter = 0;
//                    // updateWatchTime(videoType, String.valueOf(model.getId()), player.getCurrentPosition());
//                } else {
//                    timeCounter = timeCounter + 1;
//
//                }


                if (player != null) {
                    //   Log.e("RemoteKey", "total duration ........................... " + player.getDuration());
                 //   Log.e("RemoteKey", "getDuration............. " + player.getDuration() + "  .....getContentDuration...... "
                   //         + player.getContentDuration() + "........getCurrentPosition......  " + player.getCurrentPosition());
                    if (lastUpdate == 0) {
                    //    Log.e("RemoteKey", "progress ........................... " + player.getCurrentPosition());
                        lastUpdate = player.getCurrentPosition();
                        updateWatchTime(videoType, String.valueOf(model.getId()), player.getCurrentPosition());
                    } else if (player.getCurrentPosition() - lastUpdate > 5000) {
                        if (player.getCurrentPosition() + 60000 >= player.getDuration()) {
                            updateWatchTime(videoType, String.valueOf(model.getId()), 0);
                        } else {
                            if (player.getCurrentPosition() - lastUpdate > 30000) {
                                lastUpdate = player.getCurrentPosition();
                                updateWatchTime(videoType, String.valueOf(model.getId()), player.getCurrentPosition());

                            }
                        }
                      //  Log.e("RemoteKey", "progress ........................... " + player.getCurrentPosition());
                    }
                    //    progressbar.setProgress((int) ((exoPlayer.getCurrentPosition()*100)/exoPlayer.getDuration()));
                    handler.postDelayed(runnable, 5000);
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }


    private void intiViews() {
       // progressBar = findViewById(R.id.progress_bar);
        exoPlayerView = findViewById(R.id.player_view);
        rootLayout = findViewById(R.id.root_layout);
        movieTitleTV = findViewById(R.id.movie_title);
        movieDescriptionTV = findViewById(R.id.movie_description);
        posterImageView = findViewById(R.id.poster_image_view);
        serverButton = findViewById(R.id.img_server);
        subtitleButton = findViewById(R.id.img_subtitle);
        fastForwardButton = findViewById(R.id.exo_ffwd);
        rewindButton = findViewById(R.id.exo_rew);
        liveTvTextInController = findViewById(R.id.live_tv);
        seekBarLayout = findViewById(R.id.seekbar_layout);
        if (category.equalsIgnoreCase("tv")) {
            serverButton.setVisibility(View.GONE);
            subtitleButton.setVisibility(View.GONE);
            //seekBarLayout.setVisibility(View.GONE);
            fastForwardButton.setVisibility(View.GONE);
            rewindButton.setVisibility(View.GONE);
            liveTvTextInController.setVisibility(View.VISIBLE);
        }
        if (category.equalsIgnoreCase("tvseries")) {
            serverButton.setVisibility(View.GONE);
            //hide subtitle button if there is no subtitle
            if (video != null) {
                if (video.getSubtitle().isEmpty()) {
                    subtitleButton.setVisibility(View.GONE);
                }
            } else {
                subtitleButton.setVisibility(View.GONE);
            }
        }

        if (category.equalsIgnoreCase("movie")) {
            if (model.getVideoList() != null)
                videos.clear();
            videos = model.getVideoList();
            //hide subtitle button if there is no subtitle
            if (video != null) {
                if (video.getSubtitle().isEmpty()) {
                    subtitleButton.setVisibility(View.GONE);
                }
            } else {
                subtitleButton.setVisibility(View.GONE);
            }
            if (videos != null) {
                if (videos.size() < 1)
                    serverButton.setVisibility(View.GONE);
            }

        }
        serverButton.setVisibility(View.GONE);
    }


    @Override
    protected void onStart() {
        super.onStart();
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "My Tag:");

        subtitleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //open subtitle dialog
                openSubtitleDialog();

            }
        });

        serverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open server dialog
                openServerDialog(videos);
            }
        });


        //set title, description and poster in controller layout
        movieTitleTV.setText(model.getTitle());
        movieDescriptionTV.setText(model.getDescription());
        Picasso.get()
                .load(model.getCardImageUrl())
                .placeholder(R.drawable.poster_placeholder)
                .centerCrop()
                .resize(120, 200)
                .error(R.drawable.poster_placeholder)
                .into(posterImageView);

    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
                if (!exoPlayerView.isControllerVisible()) {
                    showController();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                Log.e("RemoteKey", "DPAD_DOWN");
                if (!exoPlayerView.isControllerVisible()) {
                    showController();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                Log.e("RemoteKey", "DPAD_RIGHT");
                if (!exoPlayerView.isControllerVisible()) {
                    showController();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                Log.e("RemoteKey", "DPAD_LEFT");
                if (!exoPlayerView.isControllerVisible()) {
                    showController();
                }
                break;
            case KeyEvent.KEYCODE_DPAD_CENTER:
                Log.e("RemoteKey", "DPAD_CENTER");
                if (!exoPlayerView.isControllerVisible()) {
                    showController();
                }
                break;
            case KeyEvent.KEYCODE_BACK:
                Log.e("RemoteKey", "DPAD_BACK");
                if (exoPlayerView.isControllerVisible()) {
                    hideController();
                } else {
                    if (doubleBackToExitPressedOnce) {
                        releasePlayer();
                        finish();
                    } else {
                        handleBackPress();
                    }
                }

                break;
            case KeyEvent.KEYCODE_ESCAPE:
                Log.e("RemoteKey", "DPAD_ESCAPE");
               /* if (!exoPlayerView.isControllerVisible()){
                    exoPlayerView.showController();
                }else {
                    releasePlayer();
                    finish();
                }*/
                break;
        }
        return false;
    }


    private void hideController() {

        exoPlayerView.hideController();
    }

    private void showController() {
        controllerShowingTIme = System.currentTimeMillis();
        exoPlayerView.showController();
    }

    private void handleBackPress() {
        this.doubleBackToExitPressedOnce = true;
        //Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();
        new ToastMsg(PlayerActivity.this).toastIconSuccess("Please click BACK again to exit.");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    private void openServerDialog(List<Video> videos) {
        if (videos != null) {
            List<Video> videoList = new ArrayList<>();
            videoList.clear();

            for (Video video : videos) {
                if (!video.getFileType().equalsIgnoreCase("embed")) {
                    videoList.add(video);
                }
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(PlayerActivity.this);
            View view = LayoutInflater.from(PlayerActivity.this).inflate(R.layout.layout_server_tv, null);
            RecyclerView serverRv = view.findViewById(R.id.serverRv);
            ServerAdapter serverAdapter = new ServerAdapter(PlayerActivity.this, videoList, "movie");
            serverRv.setLayoutManager(new LinearLayoutManager(PlayerActivity.this));
            serverRv.setHasFixedSize(true);
            serverRv.setAdapter(serverAdapter);

            Button closeBt = view.findViewById(R.id.close_bt);

            builder.setView(view);

            final AlertDialog dialog = builder.create();
            dialog.show();

            closeBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            final ServerAdapter.OriginalViewHolder[] viewHolder = {null};
            serverAdapter.setOnItemClickListener(new ServerAdapter.OnItemClickListener() {

                @Override
                public void onItemClick(View view, Video obj, int position, ServerAdapter.OriginalViewHolder holder) {
                    Intent playerIntent = new Intent(PlayerActivity.this, PlayerActivity.class);
                    PlaybackModel video = new PlaybackModel();
                    video.setId(model.getId());
                    video.setTitle(model.getTitle());
                    video.setDescription(model.getDescription());
                    video.setCategory("movie");
                    video.setVideo(obj);
                    video.setVideoList(model.getVideoList());
                    video.setVideoUrl(obj.getFileUrl());
                    video.setVideoType(obj.getFileType());
                    video.setBgImageUrl(model.getBgImageUrl());
                    video.setCardImageUrl(model.getCardImageUrl());
                    video.setIsPaid(model.getIsPaid());

                    playerIntent.putExtra(VideoPlaybackActivity.EXTRA_VIDEO, video);

                    startActivity(playerIntent);
                    dialog.dismiss();
                    finish();
                }
            });
        } else {
            new ToastMsg(this).toastIconError(getString(R.string.no_other_server_found));
        }
    }

    private void openSubtitleDialog() {
        if (video != null) {
            if (!video.getSubtitle().isEmpty()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PlayerActivity.this);
                View view = LayoutInflater.from(PlayerActivity.this).inflate(R.layout.layout_subtitle_dialog, null);
                RecyclerView serverRv = view.findViewById(R.id.serverRv);
                SubtitleListAdapter adapter = new SubtitleListAdapter(PlayerActivity.this, video.getSubtitle());
                serverRv.setLayoutManager(new LinearLayoutManager(PlayerActivity.this));
                serverRv.setHasFixedSize(true);
                serverRv.setAdapter(adapter);

                Button closeBt = view.findViewById(R.id.close_bt);

                builder.setView(view);
                final AlertDialog dialog = builder.create();
                dialog.show();

                closeBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                //click event
                adapter.setListener(new SubtitleListAdapter.OnSubtitleItemClickListener() {
                    @Override
                    public void onSubtitleItemClick(View view, Subtitle subtitle, int position, SubtitleListAdapter.SubtitleViewHolder holder) {


                        if (position == -1) {
                            exoPlayerView.getSubtitleView().setVisibility(View.GONE);
                            dialog.dismiss();
                            return;
                        }
                        exoPlayerView.getSubtitleView().setVisibility(VISIBLE);
                        setSelectedSubtitle(mediaSource, subtitle.getUrl());
                        dialog.dismiss();


                    }
                });

            } else {
                new ToastMsg(this).toastIconError(getResources().getString(R.string.no_subtitle_found));
            }
        } else {
            new ToastMsg(this).toastIconError(getResources().getString(R.string.no_subtitle_found));
        }
    }

    private void setSelectedSubtitle(MediaSource mediaSource, String url) {
        MergingMediaSource mergedSource;
        if (url != null) {
            Uri subtitleUri = Uri.parse(url);

            Format subtitleFormat = Format.createTextSampleFormat(
                    null, // An identifier for the track. May be null.
                    MimeTypes.TEXT_VTT, // The mime type. Must be set correctly.
                    Format.NO_VALUE, // Selection flags for the track.
                    "en"); // The subtitle language. May be null.

            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(PlayerActivity.this,
                    Util.getUserAgent(PlayerActivity.this, CLASS_NAME), new DefaultBandwidthMeter());


            MediaSource subtitleSource = new SingleSampleMediaSource
                    .Factory(dataSourceFactory)
                    .createMediaSource(subtitleUri, subtitleFormat, C.TIME_UNSET);


            mergedSource = new MergingMediaSource(mediaSource, subtitleSource);
            player.prepare(mergedSource, false, false);
            player.setPlayWhenReady(true);

            //resumePlayer();

        } else {
            Toast.makeText(PlayerActivity.this, "there is no subtitle", Toast.LENGTH_SHORT).show();
        }
    }

    long fastForwordStartPosition = 0;

    public void initVideoPlayer(String url, String type) {
        if (player != null) {
            player.release();
        }

      //  progressBar.setVisibility(VISIBLE);
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new
                AdaptiveTrackSelection.Factory(bandwidthMeter);

        TrackSelector trackSelector = new
                DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance((Context) PlayerActivity.this, trackSelector);
        exoPlayerView.setPlayer(player);
        //exoPlayerView.setFastForwardIncrementMs(480000);
        //   exoPlayerView.setRewindIncrementMs(480000);
        // exoPlayerView.setShowBuffering(PlayerView.SHOW_BUFFERING_ALWAYS);

        // below 2 lines will make screen size to fit
        /*exoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);*/

        player.setPlayWhenReady(true);

        Uri uri = Uri.parse(url);

        switch (type) {
            case "hls":
                mediaSource = hlsMediaSource(uri, PlayerActivity.this);
                break;
            case "youtube":
                extractYoutubeUrl(url, PlayerActivity.this, 18);
                break;
            case "youtube-live":
                extractYoutubeUrl(url, PlayerActivity.this, 133);
                break;
            case "rtmp":
                mediaSource = rtmpMediaSource(uri);
                break;
            default:
                mediaSource = mediaSource(uri, PlayerActivity.this);
                break;
        }

        if (!type.contains("youtube")) {
            player.prepare(mediaSource, true, false);
            exoPlayerView.setPlayer(player);
            player.setPlayWhenReady(true);
        }
        seekToStartPosition();

        // exoPlayerView.
        //  PlaybackParameters param = new PlaybackParameters(2.0f);
        // player.setPlaybackParameters(param);

//
//        DefaultTimeBar d = findViewById(R.id.exo_progress);
//
//        d.addListener(new TimeBar.OnScrubListener() {
//            @Override
//            public void onScrubStart(TimeBar timeBar, long position) {
//                Log.e("progresslistener", "........................onScrubStart " + position);
//                fastForwordStartPosition = position;
//                seekToStartPosition(fastForwordStartPosition);
//                exoPlayerView.setFastForwardIncrementMs(5000);
//                exoPlayerView.setRewindIncrementMs(5000);
//                // player.seekTo(fastForwordStartPosition+15000);
//            }
//
//            @Override
//            public void onScrubMove(TimeBar timeBar, long position) {
//                Log.e("progresslistener", "........................onScrubMove" + fastForwordStartPosition);
//                //  player.seekTo(fastForwordStartPosition+15000);
//                seekToStartPosition(fastForwordStartPosition);
//                // timeBar.setDuration(fastForwordStartPosition+15000);
//                d.setBufferedPosition(500000);
//
//            }
//
//            @Override
//            public void onScrubStop(TimeBar timeBar, long position, boolean canceled) {
//                Log.e("progresslistener", "........................onScrubStop" + position);
//                player.seekTo(fastForwordStartPosition + 15000);
//
//                // timeBar.setDuration(fastForwordStartPosition+15000);
//            }
//        });

        player.addListener(new Player.EventListener() {
            WatchNextAdapter watchNextAdapter = new WatchNextAdapter();


            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {


                if (playWhenReady && playbackState == Player.STATE_READY) {
                  //  Log.e("onPlayerStateChanged", "........................STATE_READY");
                    isPlaying = true;
                  //  progressBar.setVisibility(View.GONE);


//                    exoPlayerView.setFastForwardIncrementMs(5000);
//                    exoPlayerView.setRewindIncrementMs(5000);


                    if (!isAlreadyTimerStarted) {
                        isAlreadyTimerStarted = true;
                        startTimer();
                        controllerShowingTIme = System.currentTimeMillis();
                    }
                    /*createChannel();
                    //create media session
                    mediaSessionHelper = new MediaSessionHelper(player, PlayerActivity.this, model, isPlaying);
                    mediaSessionHelper.updateMetadata();
                    mediaSessionHelper.updatePlaybackState();*/

                } else if (playbackState == Player.STATE_READY) {
                 //   Log.e("onPlayerStateChanged", "........................STATE_READY");
                    isPlaying = false;
                  //  progressBar.setVisibility(View.GONE);
                    //add watch next card
                    long position = player.getCurrentPosition();
                    long duration = player.getDuration();
                    // mediaSessionHelper.updateMetadata();
                    // mediaSessionHelper.updatePlaybackState();

                    // watchNextAdapter.updateProgress(PlayerActivity.this, mChannelId, model, position, duration);

                } else if (playbackState == Player.STATE_BUFFERING) {
                 //   Log.e("onPlayerStateChanged", "........................STATE_BUFFERING");
                    isPlaying = false;
                  //  progressBar.setVisibility(VISIBLE);
                    player.setPlayWhenReady(true);

                } else if (playbackState == Player.STATE_ENDED) {
                //    Log.e("onPlayerStateChanged", "........................STATE_ENDED");
                  //  Log.e("123456", "ended");
                    //   getSingleEpisodData(model.getCategory(), String.valueOf(model.getId()), seasonId);
                    if (seasonId != null)
                        getSingleEpisodData(model.getCategory(), String.valueOf(model.getId()), seasonId);
                    else
                        finish();


                    //remove now playing card
                    //mediaSessionHelper.updatePlaybackState();
                    //mediaSessionHelper.stopMediaSession();

                    //watchNextAdapter.removeFromWatchNext(PlayerActivity.this, mChannelId, model.getId());
                } else {
                //    Log.e("onPlayerStateChanged", "........................STATE_READY");
                    // player paused in any state
                    isPlaying = false;
                //    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSeekProcessed() {
                //   Log.e("progresslistener", "........................onSeekProcessed");
                // player.seekTo(player.getCurrentPosition()+150000);
                // seekToStartPosition(fastForwordStartPosition);
            }
        });
        //   exoPlayerView.onF
        exoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                visible = visibility;
            }
        });


    }

    private void seekToStartPosition() {
        //  player.seekTo(325250);
        // Skip ahead if given a starting position.
        if (mStartingPosition > -1L) {
            //    if (position > -1L) {
            if (player.getPlayWhenReady()) {
              //  Log.d("VideoFragment", "Is prepped, seeking to " + mStartingPosition);
                player.seekTo(mStartingPosition);


            }
        }
    }


    private MediaSource mp3MediaSource(Uri uri) {
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), "ExoplayerDemo");
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        Handler mainHandler = new Handler();
        return new ExtractorMediaSource(uri, dataSourceFactory, extractorsFactory, mainHandler, null);
    }

    private MediaSource mediaSource(Uri uri, Context context) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory("exoplayer")).
                createMediaSource(uri);
    }

    private MediaSource rtmpMediaSource(Uri uri) {
        MediaSource videoSource = null;

        RtmpDataSourceFactory dataSourceFactory = new RtmpDataSourceFactory();
        videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);

        return videoSource;
    }

    @SuppressLint("StaticFieldLeak")
    private void extractYoutubeUrl(String url, final Context context, final int tag) {

        new YouTubeExtractor(context) {
            @Override
            public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                if (ytFiles != null) {
                    int itag = tag;
                    String dashUrl = ytFiles.get(itag).getUrl();

                    try {
                        MediaSource source = mediaSource(Uri.parse(dashUrl), context);
                        player.prepare(source, true, false);
                        //player.setPlayWhenReady(false);
                        exoPlayerView.setPlayer(player);
                        player.setPlayWhenReady(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.extract(url, true, true);

    }

    private MediaSource hlsMediaSource(Uri uri, Context context) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
                Util.getUserAgent(context, "oxoo"), bandwidthMeter);

        MediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);
        return videoSource;
    }

    @Override
    public void onBackPressed() {
        if (visible == View.GONE) {
            exoPlayerView.showController();
        } else {
            releasePlayer();
            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // Log.e(TAG, "onDestroy");
        releasePlayer();
        if (wakeLock != null)
            wakeLock.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
      //  Log.e(TAG, "onPause");
         releasePlayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
       // Log.e(TAG, "onResume");
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        wakeLock.acquire(10 * 60 * 1000L /*10 minutes*/);
    }

    private void releasePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.stop();
            player.release();
            player = null;
            exoPlayerView.setPlayer(null);

        }
    }

    boolean channelExists;
    private static final int MAKE_BROWSABLE_REQUEST_CODE = 9001;

    private void createChannel() {
        Subscription movieSubscription =
                MockDatabase.getVideoSubscription(this);
        channelExists = movieSubscription.getChannelId() > 0L;
        new AddChannelTask(this).execute(movieSubscription);
    }

    @SuppressLint("StaticFieldLeak")
    private class AddChannelTask extends AsyncTask<Subscription, Void, Long> {
        private final Context context;

        public AddChannelTask(Context context) {
            this.context = context;
        }

        @Override
        protected Long doInBackground(Subscription... varArgs) {
            List<Subscription> subscriptions = Arrays.asList(varArgs);
            if (subscriptions.size() != 1) {
                return -1L;
            }
            Subscription subscription = subscriptions.get(0);
            long channelId = TvUtil.createChannel(context, subscription);
            subscription.setChannelId(channelId);
            MockDatabase.saveSubscription(context, subscription);

            TvUtil.scheduleSyncingProgramsForChannel(getApplicationContext(), channelId);
            return channelId;
        }

        @Override
        protected void onPostExecute(Long channelId) {
            super.onPostExecute(channelId);
            if (!channelExists)
                promptUserToDisplayChannel(channelId);
        }
    }

    private void promptUserToDisplayChannel(long channelId) {
        Intent intent = new Intent(TvContractCompat.ACTION_REQUEST_CHANNEL_BROWSABLE);
        intent.putExtra(TvContractCompat.EXTRA_CHANNEL_ID, channelId);
        try {
            this.startActivityForResult(intent, MAKE_BROWSABLE_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
          //  Log.e(TAG, "Could not start activity: " + intent.getAction(), e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Toast.makeText(this, "Channel Added", Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(this, "Channel not added", Toast.LENGTH_LONG).show();
        }
    }

    private void updateWatchTime(String vtype, String vId, long duration) {


        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        DetailsApi api = retrofit.create(DetailsApi.class);
        //Call<UpdateWatchTimeResponse> call = api.updateWatchTime(Config.API_KEY, vtype, "", vId, duration);
      //  Log.e("playerActivity", ".................duration  " + duration + " seasonId  " + seasonId + " episodid  " + String.valueOf(model.getId()));
        Call<String> call = api.updateWatchTime(Config.API_KEY, PreferenceUtils.getUserId(this), category.equalsIgnoreCase("tvseries") ? model.getvId() : vId, String.valueOf(duration),
                seasonId, String.valueOf(model.getId()));
        call.enqueue(new Callback<String>() {


            @Override
            public void onResponse(Call<String> call, Response<String> response) {
             //   Log.e("playerActivity", ".................res " + response.code());
                if (response.code() == 200) {


                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //  Log.e("playerActivity", ".................res " + response.body());
            }
        });

    }


    private void getSingleEpisodData(String type, final String epId, String sId) {
        //Log.e("DetailsScreenAction", "user id : " + PreferenceUtils.getUserId(this));
      //  Log.e("DetailsScreenAction", "viceo id : " + epId);


        Retrofit retrofit = RetrofitClient.getRetrofitInstance();
        DetailsApi api = retrofit.create(DetailsApi.class);
        Call<SinglePlayBack> call = api.getSingleEpisod(Config.API_KEY, type, epId, sId);
        // Call<SinglePlayBack> call = api.getSingleEpisod(Config.API_KEY, type, epId, seasonId, PreferenceUtils.getUserId(this));
        call.enqueue(new Callback<SinglePlayBack>() {
            @Override
            public void onResponse(Call<SinglePlayBack> call, Response<SinglePlayBack> response) {
              //  Log.e("playeractivity", ".................. getSingleEpisodData....................." + response.body());
                if (response.code() == 200) {
                    SinglePlayBack video = new SinglePlayBack();
                    video = response.body();
                    //   Video videoModel = new Video();
//                    model = new PlaybackModel(
//                            Long.parseLong(singleDetails.getEpisodes_id()), null, singleDetails.getTitle(), singleDetails.getDescription()
//                            , singleDetails.getImage_url(), singleDetails.getPoster_url(), singleDetails.getFile_url(), singleDetails.getFile_type(),
//                            "tvseries", null, singleDetails.getIs_paid(), videoModel, 0, 0
//                    );
//                    Log.e("playeractivity", ".................. getSingleEpisodData....................." + singleDetails.getEpisodes_name());
//                    //    Log.e("playeractivity", ".................. getSingleEpisodData....................." + new Gson().toJson(singleDetails));


                    if (video.getEpisodes_id() != null && video.getEpisodes_id().length() > 0) {
                        PlaybackModel model = new PlaybackModel();
                        model.setId(Long.parseLong(video.getEpisodes_id()));
                        model.setTitle(video.getTitle());
                        model.setDescription("Seasson: " + video.getSeasons_name() + "; Episode: " + video.getEpisodes_name());
                        model.setVideoType(video.getFile_type());
                        model.setCategory("tvseries");
                        model.setVideoUrl(video.getFile_url());
                        Video videoModel = new Video();
                        videoModel.setSubtitle(video.getSubtitle());
                        model.setVideo(videoModel);
                        model.setCardImageUrl(video.getPoster_url());
                        model.setBgImageUrl(video.getImage_url());
                        model.setIsPaid(video.getIs_paid());
                        model.setvId(model.getvId());
                        Intent intent = getIntent();
                        // Intent intent = new Intent(PlayerActivity.this, PlayerActivity.class);
                        intent.putExtra(VideoPlaybackActivity.EXTRA_VIDEO, model);
                        intent.putExtra(VideoPlaybackActivity.SEASON_ID, video.getSeasons_id());
                        if (video.getWatch_time() != null && video.getWatch_time().length() > 0)
                            intent.putExtra(VideoPlaybackActivity.EXTRA_POSITION, Long.valueOf(video.getWatch_time()));
                        releasePlayer();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //   finish();

//                        visible = VISIBLE;
//                        releasePlayer();
//                        onBackPressed();
                        startActivity(intent);


                    } else {
                        releasePlayer();
                        finish();
//                        visible = VISIBLE;
//                        releasePlayer();
//                        onBackPressed();


                    }
                }
            }

            @Override
            public void onFailure(Call<SinglePlayBack> call, Throwable t) {
                Log.e("playeractivity", ".................. onFailure....................." + t.getMessage());
            }
        });

    }


}


