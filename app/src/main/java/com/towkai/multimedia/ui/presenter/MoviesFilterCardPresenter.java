package com.towkai.multimedia.ui.presenter;

import android.content.res.Resources;
import android.graphics.Color;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import com.towkai.multimedia.R;
import com.towkai.multimedia.model.FIlterClass;
import com.towkai.multimedia.model.Language;


public class MoviesFilterCardPresenter extends Presenter {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        TextView view = new TextView(parent.getContext());

        Resources res = parent.getResources();
        int width = res.getDimensionPixelSize(R.dimen.year_card_width);
        int height = res.getDimensionPixelSize(R.dimen.year_card_height);

        view.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        // view.setBackgroundColor(ContextCompat.getColor(parent.getContext(), R.color.colorPrimary));
        view.setBackgroundResource(getColor());
        view.setTextColor(Color.WHITE);
        view.setGravity(Gravity.CENTER);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        FIlterClass fIlterClass = (FIlterClass) item;
        ((TextView) viewHolder.view).setText(fIlterClass.getName());
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    int count;
    private int getColor() {

        //int colorList[] = {R.color.colorPrimary, R.color.blue_400, R.color.indigo_400, R.color.orange_400, R.color.light_green_400, R.color.blue_grey_400};
        int[] colorList2 = {R.drawable.gradient_1, R.drawable.gradient_2, R.drawable.gradient_3, R.drawable.gradient_4, R.drawable.gradient_5, R.drawable.gradient_6};

        if (count >= 6) {
            count = 0;
        }

        int color = colorList2[count];
        count++;

        return color;

    }

}
//
//public class LanguageCardPresenter extends Presenter {
//
// //   private static int CARD_WIDTH = 185;
//    private static int CARD_HEIGHT = 278;
//
//
//    @SuppressLint("StaticFieldLeak")
//    private static Context mContext;
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent) {
//        Log.d("onCreateViewHolder", "creating viewholder");
//        mContext = parent.getContext();
//        // ImageCardView cardView = new ImageCardView(mContext);
//        View  cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_language, parent, false);
//
//        cardView.setFocusable(true);
//        cardView.setFocusableInTouchMode(true);
//        cardView.requestLayout();
////        cardView.setInfoVisibility(View.VISIBLE);
//
//        return new ViewHolder(cardView);
//    }
//
//    @Override
//    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
//        Language language = (Language) item;
//        //  ((ViewHolder) viewHolder).mCardView.setMainImageDimensions(CARD_WIDTH, CARD_HEIGHT);
//       // ((ViewHolder) viewHolder).updateCardViewImage(video.getThumbnailUrl());
//        ((ViewHolder) viewHolder).tvName.setText((language.getName()));
//
////        ((ViewHolder) viewHolder).mCardView.setContentText(video.getDescription());
////        ((ViewHolder) viewHolder).updateContent(video.getThumbnailUrl());
////        ((ViewHolder) viewHolder).mCardView.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
//
//    }
//
//    @Override
//    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
//
//    }
//
//
//    public static class ViewHolder extends Presenter.ViewHolder {
//
//
//        ImageView imageView;
//        TextView tvName;
//        private View mCardView;
//        private Drawable mDefaultCardImage;
//        private PicassoImageCardViewTarget mImageCardViewTarget;
//
//        @SuppressLint("UseCompatLoadingForDrawables")
//        public ViewHolder(View view) {
//            super(view);
//            mCardView = view;
//            imageView=view.findViewById(R.id.imageView);
//            tvName =view.findViewById(R.id.btnLanguageName);
////            mImageCardViewTarget = new PicassoImageCardViewTarget(mCardView);
//            mDefaultCardImage = mContext
//                    .getResources()
//                    .getDrawable(R.drawable.logo);
//        }
//
//        public View getCardView() {
//            return mCardView;
//        }
//
//        protected void updateCardViewImage(String url) {
//
//            Picasso.get()
//                    .load(url)
//                    .placeholder(R.drawable.poster_placeholder)
//                    .error(mDefaultCardImage)
//                    .into(imageView);
//        }
//        protected void updateContent(String url) {
//            //Log.e("cardpresenter","Card Presenter .........................................."+mCardView.getTitleText());
//            //  mCardView.setTitleText(url);
//        }
//    }
//
//
//    static class PicassoImageCardViewTarget implements Target {
//
//
//        private ImageCardView mImageCardView;
//
//        public PicassoImageCardViewTarget(ImageCardView mImageCardView) {
//            this.mImageCardView = mImageCardView;
//        }
//
//        @Override
//        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//            Drawable bitmapDrawable = new BitmapDrawable(mContext.getResources(), bitmap);
//            mImageCardView.setMainImage(bitmapDrawable);
//        }
//
//        @Override
//        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//            mImageCardView.setMainImage(errorDrawable);
//        }
//
//
//        @Override
//        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//        }
//    }
//
//}
//
//





