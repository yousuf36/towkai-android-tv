package com.towkai.multimedia.ui.activity;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.towkai.multimedia.R;

public class ItemGenreActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_genre);
    }
}
