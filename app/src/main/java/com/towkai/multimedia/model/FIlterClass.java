package com.towkai.multimedia.model;

public class FIlterClass {

   private String id, name, iso;


    public FIlterClass(String id,String name, String key) {
 this.id=id;
        this.name = name;
        this.iso = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }
}
