package com.towkai.multimedia.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SinglePlayBack implements Serializable {

    @SerializedName("videos_id")
    @Expose
    private String videos_id;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("is_tvseries")
    @Expose
    private String is_tvseries;
    @SerializedName("is_paid")
    @Expose
    private String is_paid;

    @SerializedName("poster_url")
    @Expose
    private String poster_url;


    @SerializedName("seasons_id")
    @Expose
    private String seasons_id;

    @SerializedName("seasons_name")
    @Expose
    private String seasons_name;

    @SerializedName("episodes_id")
    @Expose
    private String episodes_id;

    @SerializedName("episodes_name")
    @Expose
    private String episodes_name;

    @SerializedName("stream_key")
    @Expose
    private String stream_key;

    @SerializedName("file_type")
    @Expose
    private String file_type;

    @SerializedName("image_url")
    @Expose
    private String image_url;


    @SerializedName("file_url")
    @Expose
    private String file_url;


    @SerializedName("watch_time")
    @Expose
    private String watch_time;

    @SerializedName("subtitle")
    @Expose
    private List<Subtitle> subtitle = null;


    public List<Subtitle> getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(List<Subtitle> subtitle) {
        this.subtitle = subtitle;
    }

    public String getWatch_time() {
        return watch_time;
    }

    public void setWatch_time(String watch_time) {
        this.watch_time = watch_time;
    }

    public String getVideos_id() {
        return videos_id;
    }

    public void setVideos_id(String videos_id) {
        this.videos_id = videos_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIs_tvseries() {
        return is_tvseries;
    }

    public void setIs_tvseries(String is_tvseries) {
        this.is_tvseries = is_tvseries;
    }

    public String getIs_paid() {
        return is_paid;
    }

    public void setIs_paid(String is_paid) {
        this.is_paid = is_paid;
    }

    public String getPoster_url() {
        return poster_url;
    }

    public void setPoster_url(String poster_url) {
        this.poster_url = poster_url;
    }

    public String getSeasons_id() {
        return seasons_id;
    }

    public void setSeasons_id(String seasons_id) {
        this.seasons_id = seasons_id;
    }

    public String getSeasons_name() {
        return seasons_name;
    }

    public void setSeasons_name(String seasons_name) {
        this.seasons_name = seasons_name;
    }

    public String getEpisodes_id() {
        return episodes_id;
    }

    public void setEpisodes_id(String episodes_id) {
        this.episodes_id = episodes_id;
    }

    public String getEpisodes_name() {
        return episodes_name;
    }

    public void setEpisodes_name(String episodes_name) {
        this.episodes_name = episodes_name;
    }

    public String getStream_key() {
        return stream_key;
    }

    public void setStream_key(String stream_key) {
        this.stream_key = stream_key;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }
}
